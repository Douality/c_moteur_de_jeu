#pragma once

bool In_AABB(const AABB aabb_object, const AABB aabb_in){
    return (aabb_object.max.x >= aabb_in.min.x && aabb_object.min.x <= aabb_in.max.x) && 
            (aabb_object.max.y >= aabb_in.min.y && aabb_object.min.y <= aabb_in.max.y) && 
            (aabb_object.max.z >= aabb_in.min.z && aabb_object.min.z <= aabb_in.max.z);
};

enum Espace{
    OBJECT,
    WORLD
};

struct Physics{
    float coeff_vitesse;
    float coeff_friction;
    float coeff_gravity;

    float cellule_vitesse(const float& v){
        return v*coeff_vitesse;
    }
    float cellule_friction(const float& f){
        return f*coeff_friction;
    }
    float cellule_gravity(const float& g){
        return g*coeff_gravity;
    }

    glm::vec3 vitesse(const glm::vec3& v){
        return v*coeff_vitesse;
    }
    glm::vec3 friction(const glm::vec3& v){
        return v*coeff_friction;
    }
    glm::vec3 gravite(const glm::vec3& v){
        return v*coeff_gravity;
    }

    glm::vec3 vitesse(const glm::vec3& v,const float& vit){
        return v*cellule_vitesse(vit);
    }
    glm::vec3 friction(const glm::vec3& v, const float& fric){
        return v*cellule_friction(fric);
    }
    glm::vec3 gravite(const glm::vec3& v, const float& grav){
        return v*cellule_gravity(grav);
    }

    Physics(){}
    ~Physics(){}
    Physics(const float& v, const float& f, const float& g) : 
        coeff_vitesse(v) , coeff_friction(f) , coeff_gravity(g) {}
};

void ReposPerspectiveCamera(glm::vec3& mini, glm::vec3& maxi){
    if(mini.x > maxi.x){
        float swapper = mini.x;
        mini.x = maxi.x;
        maxi.x = swapper;
    }
    if(mini.y > maxi.y){
        float swapper = mini.y;
        mini.y = maxi.y;
        maxi.y = swapper;
    }
    if(mini.z > maxi.z){
        float swapper = mini.z;
        mini.z = maxi.z;
        maxi.z = swapper;
    }
};

void ReposPerspectiveCamera(AABB& aabb){
    glm::vec3 *mini, *maxi;
    mini = &(aabb.min);
    maxi = &(aabb.max);
    if(mini->x > maxi->x){
        float swapper = mini->x;
        mini->x = maxi->x;
        maxi->x = swapper;
    }
    if(mini->y > maxi->y){
        float swapper = mini->y;
        mini->y = maxi->y;
        maxi->y = swapper;
    }
    if(mini->z > maxi->z){
        float swapper = mini->z;
        mini->z = maxi->z;
        maxi->z = swapper;
    }
};

class Cellule{
    public:
        Physics physics;
        AABB boite;
        GLuint index_texture;
        GLuint atlas_map;
        std::vector<Node*> elements;
        Cellule(){}
        ~Cellule(){}
        Cellule(const float& v, const float& f, const float& g){
            physics.coeff_vitesse=v;
            physics.coeff_friction=f;
            physics.coeff_gravity=g;
        }

        void set_bordure_min(glm::vec3 m){
            boite.min = m;
            ReposPerspectiveCamera(boite.min,boite.max);
        }
        void set_bordure_max(glm::vec3 m){
            boite.max = m;
            ReposPerspectiveCamera(boite.min,boite.max);
        }
        void set_aabb(AABB aabb){
            boite.min = aabb.min;
            boite.max = aabb.max;
            ReposPerspectiveCamera(boite.min,boite.max);
        }
        void set_aabb(glm::vec3 mini, glm::vec3 maxi){
            boite.min = mini;
            boite.max = maxi;
            ReposPerspectiveCamera(boite.min,boite.max);
        }

        glm::vec3 bordure_min(){
            return boite.min;
        }
        glm::vec3 bordure_max(){
            return boite.max;
        }

        void reposCoordinateWorld(const glm::mat4& viewMatrix, 
                                    const glm::mat4& projectionMatrix, 
                                    const glm::mat4& model_Matrix = glm::mat4(1.0f))
        {
            boite.min = glm::vec3(projectionMatrix*viewMatrix*model_Matrix * glm::vec4(boite.min,1.0f));
            boite.max = glm::vec3(projectionMatrix*viewMatrix*model_Matrix * glm::vec4(boite.max,1.0f));
            ReposPerspectiveCamera(boite.min,boite.max);
        }

        void clean(){
            elements.resize(0);
        }
        void add(Node* n){
            elements.push_back(n);
        }
        int nb_elements(){
            return elements.size();
        }
};

struct Grille{  
    Espace espace;
    AABB grid_aabb;

    //Dynamique
    std::vector<Cellule*> cellules;
    //Static
    std::vector<std::vector<std::vector<Cellule*>>> grille;

    std::vector<void*>elem;

    Grille(){
        grid_aabb.min = glm::vec3(-FLT_MAX,-FLT_MAX,-FLT_MAX);
        grid_aabb.max = glm::vec3(FLT_MAX,FLT_MAX,FLT_MAX);
        espace = OBJECT;
    }
    Grille(int option = 0){
        glm::vec3 max_border, min_border;
        if(option == 0){
            max_border = glm::vec3(-FLT_MAX,-FLT_MAX,-FLT_MAX);
            min_border = glm::vec3(FLT_MAX,FLT_MAX,FLT_MAX);
        }
        else{
            min_border = glm::vec3(-FLT_MAX,-FLT_MAX,-FLT_MAX);
            max_border = glm::vec3(FLT_MAX,FLT_MAX,FLT_MAX);
        }
        grid_aabb.min = min_border;
        grid_aabb.max = max_border;
        espace = OBJECT;
    }
    Grille(glm::vec3 mini, glm::vec3 maxi){
        grid_aabb.max = maxi;
        grid_aabb.min = mini;
        ReposPerspectiveCamera(grid_aabb.min,grid_aabb.max);
        espace = OBJECT;
    }
    Grille(AABB aabb){
        grid_aabb.max = aabb.max;
        grid_aabb.min = aabb.min;
        ReposPerspectiveCamera(grid_aabb.min,grid_aabb.max);
        espace = OBJECT;
    }
    ~Grille(){}

    void set_Grille(const Node* node, int op = 0){
        glm::vec3 max_grid = this->grid_aabb.max;
        glm::vec3 min_grid = this->grid_aabb.min;
        if(op = 0){
            //std::cout << "Node as -> " << node->indexed_vertices.size() << std::endl;
            for(glm::vec3 v : node->indexed_vertices){
                if(v.x < min_grid.x) min_grid.x = v.x;
                if(v.x > max_grid.x) max_grid.x = v.x;
                if(v.y < min_grid.y) min_grid.y = v.y;
                if(v.y > max_grid.y) max_grid.y = v.y;
                if(v.z < min_grid.z) min_grid.z = v.z;
                if(v.z > max_grid.z) max_grid.z = v.z;
            }
        }
        else{
            if(node->aabb.min.x < min_grid.x) min_grid.x = node->aabb.min.x;
            if(node->aabb.max.x > max_grid.x) max_grid.x = node->aabb.max.x;
            if(node->aabb.min.y < min_grid.y) min_grid.y = node->aabb.min.y;
            if(node->aabb.max.y > max_grid.y) max_grid.y = node->aabb.max.y;
            if(node->aabb.min.z < min_grid.z) min_grid.z = node->aabb.min.z;
            if(node->aabb.max.z > max_grid.z) max_grid.z = node->aabb.max.z;
        }
        this->grid_aabb.min = min_grid;
        this->grid_aabb.max = max_grid;
        for(Node* n : node->children){
            set_Grille(n);
        }
    };

    void reposCoordinateWorld(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const glm::mat4 model_Matrix = glm::mat4(1.0f)){
        glm::mat4 mvc = projectionMatrix*viewMatrix*model_Matrix;
        grid_aabb.min = glm::vec3(mvc * glm::vec4(grid_aabb.min,1.0f));
        grid_aabb.max = glm::vec3(mvc * glm::vec4(grid_aabb.max,1.0f));
        ReposPerspectiveCamera(grid_aabb.min,grid_aabb.max);
        for(std::vector<std::vector<Cellule*>>& gridX : grille){
            for(std::vector<Cellule*>& gridXY : gridX){
                for(Cellule* cell : gridXY){
                    cell->boite.min = (glm::vec3(mvc*glm::vec4(cell->boite.min,1.0f)));
                    cell->boite.max = (glm::vec3(mvc*glm::vec4(cell->boite.max,1.0f))); 
                    ReposPerspectiveCamera(cell->boite.min,cell->boite.max);
                }
            }
        }
        espace = WORLD;
    }

    Cellule* find_cell(Node* node){
        bool place = false;
        for(std::vector<std::vector<Cellule*>>& gridX : grille){
            for(std::vector<Cellule*>& gridXY : gridX){
                for(Cellule* cell : gridXY){
                    if(In_AABB(node->aabb,cell->boite)){
                        cell->add(node);
                        place = true;
                        return cell;
                    }
                }
            }
        }
        return nullptr;
    }

    void put_node(Node* Node){
        Cellule * cell = find_cell(Node);
        if(cell != nullptr){
            int i = cell->nb_elements();
            cell->add(Node);
            assert(cell->nb_elements() > i);
        }
    }

    void put_node(Node& Node){
        Cellule * cell = find_cell(&Node);
        if(cell != nullptr){
            int i = cell->nb_elements();
            cell->add(&Node);
            assert(cell->nb_elements() > i);
        }
    }

    void add_Object(Node* node, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const glm::mat4 &parent_transform = glm::mat4(1.0f)){
        glm::mat4 model_matrix = parent_transform * node->transform.getMatrix();
        glm::mat4 mvc = projectionMatrix*viewMatrix*model_matrix;
        AABB object_aabb;
        object_aabb.min = glm::vec3(mvc*glm::vec4(node->aabb.min,1.0f));
        object_aabb.max = glm::vec3(mvc*glm::vec4(node->aabb.max,1.0f));
        ReposPerspectiveCamera(object_aabb.min,object_aabb.max);

        if(espace != WORLD) reposCoordinateWorld(viewMatrix,projectionMatrix);
        bool place = false;
        for ( int i = 0; i<grille.size() && !place; i++){
            for ( int j = 0; i<grille[i].size() && !place; j++){
                for ( int z = 0; z <grille[i][j].size() && !place; z++){
                    if(In_AABB(object_aabb,grille[i][j][z]->boite)){
                        grille[i][j][z]->add(node);
                        place = true;
                    }
                }
            }
        }
    }

    void add_hierachie(Node* node, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const glm::mat4 &parent_transform = glm::mat4(1.0f)){
        add_Object(node,viewMatrix,projectionMatrix,parent_transform);
        for(Node* n : node->children){
            add_hierachie(n,viewMatrix,projectionMatrix,parent_transform * node->transform.getMatrix());
        }
    }

    void clean(){
        grille = std::vector<std::vector<std::vector<Cellule*>>>();
        assert(grille.size() <= 0);
    }

    void print_grid(){
        std::cout << "Border are : [( " << grid_aabb.min.x << "," << grid_aabb.min.y << "," << grid_aabb.min.z 
                  << " ),( " << grid_aabb.max.x << "," << grid_aabb.max.y << "," << grid_aabb.max.z 
                  << " )]"<< std::endl;
        std::cout << " Grid as : " << grille.size() << std::endl;
        std::cout << "End Grid" << std::endl;
    }
    void print_grid(int i){
        std::cout << "Grid[" << i <<"] as : " <<grille[i].size()<<" elements"<<std::endl;
    }
    void print_grid(int i, int j){
        std::cout << "Grid[" << i <<","<< j <<"] as : " <<grille[i][j].size()<<" elements"<<std::endl;
    }

    void add_cell(int x, int y, Cellule* c){
        grille[x][y].push_back(c);
    }
    Cellule* get_cell(int i, int j, int k){
        return grille[i][j][k];
    }

    void add_cell(int x, int y, glm::vec3 mini, glm::vec3 maxi, float v, float fric, float grav){
        Cellule* c = new Cellule();
        c->elements.resize(0);
        c->boite.min = mini;
        c->boite.max = maxi;
        ReposPerspectiveCamera(c->boite.min,c->boite.max);
        c->physics.coeff_vitesse = v;
        c->physics.coeff_friction = fric;
        c->physics.coeff_gravity = grav;
        add_cell(x,y,c);
    }
    void add_cell(int x, int y, AABB aabb, float v, float fric, float grav){
        Cellule* c = new Cellule();
        c->elements.resize(0);
        c->boite.min = aabb.min;
        c->boite.max = aabb.max;
        ReposPerspectiveCamera(c->boite.min,c->boite.max);
        c->physics.coeff_vitesse = v;
        c->physics.coeff_friction = fric;
        c->physics.coeff_gravity = grav;
        add_cell(x,y,c);
    }
    void add_cell(int x, int y, AABB aabb){
        Cellule* c = new Cellule();
        c->elements.resize(0);
        c->boite.min = aabb.min;
        c->boite.max = aabb.max;
        ReposPerspectiveCamera(c->boite.min,c->boite.max);
        add_cell(x,y,c);
    }
     void add_cell(int x, int y){
        Cellule* c = new Cellule();
        ReposPerspectiveCamera(c->boite.min,c->boite.max);
        add_cell(x,y,c);
    }
    void add_layer(){
        grille.push_back(std::vector<std::vector<Cellule*>>());
    }
    void add_layer(int x){
        grille[x].push_back(std::vector<Cellule*>());
    }
    void add_layer(int x, int y){
        Cellule* c = new Cellule();
        grille[x][y].push_back(c);
    }
    void push_cell(){
        grille.push_back(std::vector<std::vector<Cellule*>>());
        grille[grille.size()-1].push_back(std::vector<Cellule*>());
    }
    void push_cell(int i){
        if(i > grille.size()-1) {
            while(i > grille.size() - 1){
                grille.push_back(std::vector<std::vector<Cellule*>>());
            }
        }
        grille[i].push_back(std::vector<Cellule*>());
    }
    void push_cell(int i, int j){
        if(i > grille.size()-1) {
            while(i > grille.size() - 1){
                grille.push_back(std::vector<std::vector<Cellule*>>());
            }
            grille[i].push_back(std::vector<Cellule*>());
        }
        if(j > grille[i].size()-1){
            while(j > grille[i].size()-1){
                grille[i].push_back(std::vector<Cellule*>());
            }
        }
        Cellule * cell = new Cellule();
        grille[i][j].push_back(cell);
    }
    void push_cell(int i, int j, int k){
        if(i > grille.size()-1) {
            while(i > grille.size() - 1){
                grille.push_back(std::vector<std::vector<Cellule*>>());
            }
            grille[i].push_back(std::vector<Cellule*>());
        }
        if(j > grille[i].size()-1){
            while(j > grille[i].size()-1){
                grille[i].push_back(std::vector<Cellule*>());
            }
        }
        if(k > grille[i][j].size()-1){
            while(k > grille[i][j].size()-1){
                Cellule * cell = new Cellule();
                grille[i][j].push_back(cell);
            }
        }
        Cellule * cell = new Cellule();
        grille[i][j][k] = cell;
    }

    void add_node(int x, int y, int z, Node* node){
        grille[x][y][z]->add(node);
    }
};

Grille Grid(0);

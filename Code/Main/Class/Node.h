#pragma once
struct Zone{
    glm::vec3 min;
    glm::vec3 max;
    float profondeur;
    Zone(){};
    Zone(glm::vec3 mini, glm::vec3 maxi, float p){
        min = mini;
        max = maxi;
        profondeur = p;
    }
    Zone(float a, float b, float c, float d, float e, float f, float p){
        min = glm::vec3(a,b,c);
        max = glm::vec3(d,e,f);
        profondeur = p;
    }
};

struct Node {
    bool toDisplay;
    int option;

    Material material;
    std::vector<Zone> particular_zone;
    std::vector<Node *> children;

    Transform transform;
    
    bool isSphere, isSurface, isCube, isArbre, isRectangle, isStar, isCylindre;
    float sphereSize, cubeSize;
    std::vector<unsigned short> indices;
    std::vector <std::vector<unsigned short>> triangles;
    std::vector <glm::vec3> indexed_vertices;
    std::vector <glm::vec2> uvs;
    std::vector <glm::vec3> normals;
    CubeProperties* cubeProperties;
    AABB aabb;
    glm::vec3 surfaceOrientation;

    bool active;


    Node(Transform transform, bool isSphere, bool isSurface, bool isCube, bool isArbre, float size, CubeProperties* cubeProperties = nullptr, bool toDisplay = true)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube), isArbre(isArbre),
              sphereSize(isSphere ? size : 0.0f), cubeSize(isCube ? size : 0.0f), cubeProperties(cubeProperties), toDisplay(toDisplay), active(true) {
        aabb.min = transform.t - glm::vec3(size/2.0f) * transform.s;
        aabb.max = transform.t + glm::vec3(size/2.0f) * transform.s;
    }

    Node(Transform transform, bool isSphere, bool isSurface, bool isCube, bool isArbre, glm::vec3 surfaceOrientation, bool toDisplay = true)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube), isArbre(isArbre),
            surfaceOrientation(surfaceOrientation), toDisplay(toDisplay), active(true) {
        glm::vec3 size = transform.s;
        glm::vec3 minLocal = -size / 2.0f;
        glm::vec3 maxLocal = size / 2.0f;

        glm::vec3 minWorld = transform.t + minLocal;
        glm::vec3 maxWorld = transform.t + maxLocal;

        // Rotate the AABB
        glm::mat4 rotationMat = glm::mat4(transform.r);
        minWorld = glm::vec3(rotationMat * glm::vec4(minWorld, 1.0f));
        maxWorld = glm::vec3(rotationMat * glm::vec4(maxWorld, 1.0f));

        aabb.min = glm::min(minWorld, maxWorld);
        aabb.max = glm::max(minWorld, maxWorld);
    }


    Node(Transform transform, bool isSphere, bool isSurface, bool isCube, bool isArbre = false, bool toDisplay = true)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube), isArbre(isArbre),
              sphereSize(0.0f), cubeSize(0.0f), cubeProperties(nullptr), toDisplay(toDisplay), active(true) {
        float size = 0.0f; // Définissez une taille appropriée pour les cas où ni isSphere, ni isSurface, ni isCube ne sont vrais
        aabb.min = transform.t - glm::vec3(size/2.0f) * transform.s;
        aabb.max = transform.t + glm::vec3(size/2.0f) * transform.s;
    }



    float distanceFromCamera(const glm::vec3 &camera_position) const {
        glm::vec3 node_position = transform.t;
        return glm::length(camera_position - node_position);
    }
};

Transform getAbsoluteTransform(Node *node, Node *targetNode, const Transform &parentTransform){
    // Calcule la transformation du nœud en prenant en compte la transformation du parent.
    Transform nodeTransform = parentTransform.combine(node->transform);

    if (node == targetNode) {
        // Si le nœud actuel est le nœud cible (le bras dans votre cas), renvoie sa transformation absolue.
        //std::cout << "trouvé: " << nodeTransform.t.x << " " << nodeTransform.t.y << " " << nodeTransform.t.z << std::endl;
        return nodeTransform;
    }

    // Parcourir les enfants et récursivement calculer leur transformation absolue.
    for (Node *child : node->children) {
        Transform childTransform = getAbsoluteTransform(child, targetNode, nodeTransform);
        if (childTransform.t != glm::vec3(0.0f) || childTransform.r != glm::mat3x3(0.0f) || childTransform.s != glm::vec3(0.0f)) {
            // Si la transformation de l'enfant est différente de l'identité, cela signifie que le nœud cible a été trouvé.
            //std::cout << "trouvé enfant" << std::endl;
            return childTransform;
        }
    }

    // Si le nœud cible n'a pas été trouvé, renvoie une transformation identité.
    return Transform(glm::vec3(0.0f), glm::mat3x3(0.0f), glm::vec3(0.0f));
}

//Creation d'une zone invisible autour d'un node
//Dans le but ici d'avoir des coordonnées locals pour le Fragment Shader pour des couleurs en zone spécifiques
Node* zone_collison(glm::vec3 t, float size = 20.0f){
    Node *Zone = new Node(
            Transform(glm::vec3(size), glm::mat3x3(1.0f), t),
            true, false, false, false, 0.02f, nullptr, false);
    return Zone;
}

//Principal node
//Character
Node* body;
//Spam
Node* protection;
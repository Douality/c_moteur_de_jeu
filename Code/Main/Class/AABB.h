#pragma once
struct AABB {
    glm::vec3 min; // Coordonnées du coin inférieur gauche de la boîte
    glm::vec3 max; // Coordonnées du coin supérieur droit de la boîte

    AABB() = default;

    AABB(glm::vec3 min, glm::vec3 max) : min(min), max(max) {}
};

/**
 * une fonction pour vérifier si deux boîtes AABB sont en collision
 * @param box1
 * @param box2
 * @return true si les boîtes se chevauchent, sinon false.
 */
bool checkAABBCollision(const AABB &box1, const AABB &box2) {
    return (box1.min.x <= box2.max.x && box1.max.x >= box2.min.x) &&
           (box1.min.y <= box2.max.y && box1.max.y >= box2.min.y) &&
           (box1.min.z <= box2.max.z && box1.max.z >= box2.min.z);
}
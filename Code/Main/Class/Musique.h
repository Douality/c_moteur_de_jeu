#pragma once

//Dans un terminal, inporter les bibliothèques sdl
/*
sudo apt-get install libsdl2-mixer-dev
sudo apt-get install libsdl2-dev libsdl2-mixer-dev
sudo apt-get install libsdl-mixer1.2-dev
*/

//Ne pas oublier d'inclure dans le fichier
/*
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
*/

//J'ai pas test sans l cmake mais ce à du mal j'ai l'impression Dans le cmake
/*
target_link_libraries(TP1  ->  target_link_libraries(TP1
    ${ALL_LIBS}                   ${ALL_LIBS}
)                                 SDL2
                                  SDL2_mixer
                               )
*/
bool isPlaying = true;
bool asSwitch_music = false;
bool active_current_music = true;
bool defeat_music = false;
Mix_Music* current_music = NULL;
int volume = 6;

void init_musique(){
    Mix_Init(MIX_INIT_FLAC | MIX_INIT_MOD | MIX_INIT_MP3 | MIX_INIT_OGG);
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
}

// Fonction pour arrêter la musique
void stop_music() {
    if (Mix_PlayingMusic() != 0) {
        Mix_HaltMusic();
        Mix_FreeMusic(current_music);
    }
}

// Fonction pour jouer une nouvelle musique
void play_music(const char* filename, int power = volume, int loops = -1) {
    stop_music();

    Mix_Music* music = Mix_LoadMUS(filename);

    if (music == NULL) {
        printf("Impossible de charger la musique : %s\n", Mix_GetError());
        return;
    }

    if (Mix_PlayMusic(music, loops) == -1) {
        printf("Impossible de jouer la musique : %s\n", Mix_GetError());
        Mix_FreeMusic(music);
        return;
    }

    Mix_VolumeMusic(MIX_MAX_VOLUME / power);
    current_music = music;
}
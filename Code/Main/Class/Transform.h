#pragma once
class Transform {
public:
    glm::vec3 s; // non-uniform scale
    glm::mat3x3 r; // rotation
    glm::vec3 t; // translation
    glm::vec3 rotationCenter; // center of rotation

    Transform(glm::vec3 scale, glm::mat3x3 rotation, glm::vec3 translation)
            : s(scale), r(rotation), t(translation), rotationCenter(glm::vec3(0.0f)) {}

    Transform(glm::vec3 scale, glm::mat3x3 rotation, glm::vec3 translation, glm::vec3 center)
            : s(scale), r(rotation), t(translation), rotationCenter(center) {}

    Transform mix_with(Transform b, float k) const {
        Transform result(this->s, this->r, this->t);
        result.s = this->s * k + b.s * (1 - k);
        result.r = glm::mix(this->r, b.r, k);
        result.t = this->t * k + b.t * (1 - k);
        result.rotationCenter = this->rotationCenter * k + b.rotationCenter * (1 - k);
        return result;
    }

    Transform combine(const Transform &other) const {
        Transform result(*this);
        result.s = this->s * other.s;
        result.r = this->r * other.r;
        result.t = this->t + other.t;
        return result;
    }

    // Apply the transformation on a point
    glm::vec3 applyToPoint(glm::vec3 point) {
        glm::vec3 translatedPoint = point - rotationCenter;
        return this->s * (this->r * translatedPoint) + this->t + rotationCenter;
    }

    // Apply the transformation on a vector
    glm::vec3 applyToVector(glm::vec3 vector) {
        return this->s * (this->r * vector);
    }

    // Apply the transformation on a versor
    glm::vec3 applyToVersor(glm::vec3 versor) {
        return this->r * versor;
    }

    glm::mat4 getMatrix() {
        glm::mat4 modelMatrix = glm::mat4(1.0f);
        modelMatrix = glm::scale(modelMatrix, glm::vec3(s));
        modelMatrix = glm::translate(modelMatrix, t * glm::vec3(1.0f / s.x, 1.0f / s.y, 1.0f / s.z));
        modelMatrix *= glm::mat4(r);


        glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), rotationCenter);
        glm::mat4 inverseTranslationMatrix = glm::translate(glm::mat4(1.0f), -rotationCenter);

        modelMatrix = translationMatrix * modelMatrix * inverseTranslationMatrix;

        return modelMatrix;
    }

};
#pragma once
struct GrapheScene {
    Node *root;
    Node *barreVie;
    std::vector<Node*> pouleParts;
    GLuint shader;
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;
    std::vector<Node*> platforms;
    GLint isHealthBarLoc;
    GLint isPoulePartLoc;

    GrapheScene(Node *rootNode, GLuint shaderProgram) : root(rootNode), shader(shaderProgram) {}

    void ajoutNode(Node *node) {
        root->children.push_back(node);
    }

    void ajoutNodeAParent(Node *parentNode, Node *childNode) {
        parentNode->children.push_back(childNode);
    }

    void ajoutPlateforme(Node *node) {
        platforms.push_back(node);
        ajoutNode(node);
    }

    void supprimerNode(Node *node) {
        //supprimerNodeDeParent(root, node);
        node->active = false;
    }


    void update(const glm::mat4 &viewMatrix, const glm::mat4 &projectionMatrix) {
        this->viewMatrix = viewMatrix;
        this->projectionMatrix = projectionMatrix;
    }

    //afficher la scène
    void render(Node *node, glm::mat4 &parent_transform) {
        isHealthBarLoc = glGetUniformLocation(this->shader, "isHealthBar");
        isPoulePartLoc = glGetUniformLocation(this->shader, "isPoulePart");
        glUniform1i(isHealthBarLoc, 0);
        glUniform1i(isPoulePartLoc, 0);

        if(!node->active){
            return;
        }
        glm::mat4 model_matrix = parent_transform * node->transform.getMatrix();

        if (node == this->barreVie) {
            // Calcul de la couleur de la barre de vie
            glm::vec3 barColor = glm::vec3(HpPoule / 100.0f, 0.0f, 0.0f);;

            // Mise à jour de la couleur de la barre de vie dans le shader
            GLint barColorLoc = glGetUniformLocation(this->shader, "barColor");
            glUniform3fv(barColorLoc, 1, glm::value_ptr(barColor));
            glUniform1i(isHealthBarLoc, 1);

            glm::vec2 faceUVsHPbar[6][4] = {
                    {{1.0f, 0.0f}, {0.0f, 0.0f}, {0.0f, 1.0f}, {1.0f, 1.0f}}, // Face avant
                    {{0.0f, 0.0f}, {0.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f}}, // Face droite
                    {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face arrière
                    {{1.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {1.0f, 1.0f}}, // Face gauche
                    {{1.0f, 1.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 0.0f}}, // Face du dessus
                    {{1.0f, 1.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 0.0f}}  // Face du dessous
            };

            node->uvs.clear();

            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 4; j++) {
                    node->uvs.push_back(faceUVsHPbar[i][j]);
                }
            }
        }

        for(int i = 0; i < pouleParts.size(); i++){
            if(node == this->pouleParts.at(i)){
                glUniform1i(isPoulePartLoc, i+1);
                break;
            }
        }

        float detailFactor = 1.0f;
        float distanceFromCamera = node->distanceFromCamera(camera_position);
        if (distanceFromCamera > 10.0f) {
            detailFactor = 0.5f;
        } else if (distanceFromCamera > 20.0f) {
            detailFactor = 0.25f;
        }

        std::vector<unsigned short> indices = node->indices; //Triangles concaténés dans une liste
        std::vector <std::vector<unsigned short>> triangles = node->triangles;
        std::vector <glm::vec3> indexed_vertices = node->indexed_vertices;
        std::vector <glm::vec2> uvs = node->uvs;
        normals = node->normals;

        //affichage différent selon le type de node (appel de la fonction de création du type de node)
        if (node->isSphere) {
            newSphere(indices, triangles, indexed_vertices, uvs, normals, resolution, node->sphereSize, detailFactor);
        }
        else if (node->isSurface) {
            surfacePlane(indices, triangles, indexed_vertices, uvs, normals, 
                            (node->option==SKYBOX ? 2.0f : resolution), 
                            detailFactor,active_map_dgt);
             // Calculer les points min et max après transformation
            glm::vec4 temp_min_point = model_matrix * glm::vec4(indexed_vertices[0], 1.0f);
            glm::vec3 min_point = glm::vec3(temp_min_point);
            glm::vec3 max_point = min_point;
            for (const auto& vertex : indexed_vertices) {
                glm::vec4 temp_transformed_vertex = model_matrix * glm::vec4(vertex, 1.0f);
                glm::vec3 transformed_vertex = glm::vec3(temp_transformed_vertex);
                for (int i = 0; i < 3; ++i) {
                    min_point[i] = std::min(min_point[i], transformed_vertex[i]);
                    max_point[i] = std::max(max_point[i], transformed_vertex[i]);
                }
            }
            // Créer et assigner la nouvelle AABB
            node->aabb = AABB(min_point, max_point);
        }
        else if (node->isCube) {
            newCube(indices, triangles, indexed_vertices, uvs, normals, node->cubeSize, detailFactor, node->cubeProperties);
        }
        else if (node->isArbre) {
            Arbre(indices, triangles, indexed_vertices, uvs, normals, 2.0f, glm::vec3(1.0f), node->aabb);
        }

        if(node->option == CREUSE){
            if(node->particular_zone.size() > 0){
                for(Zone& zone : node->particular_zone){
                    creuse(zone.min.x,zone.min.y,zone.min.z,zone.max.x,zone.max.y,zone.max.z,zone.profondeur,indexed_vertices);
                }
            }
        }

        if(node->toDisplay || node->active){
        // Load it into a VBO
            GLuint vertexbuffer;
            glGenBuffers(1, &vertexbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                         GL_STATIC_DRAW);

            // Generate a buffer for the indices as well
            GLuint elementbuffer;
            glGenBuffers(1, &elementbuffer);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

            GLuint uvbuffer;
            glGenBuffers(1, &uvbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

            GLuint normalBuffer;
            glGenBuffers(1, &normalBuffer);
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glEnableVertexAttribArray(2);
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

            GLuint modelMatrixID = glGetUniformLocation(this->shader, "model");
            glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &model_matrix[0][0]);

            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void *) 0);

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glDisableVertexAttribArray(2);


            glUniform1i(glGetUniformLocation(shader, "active_phong"), 1);
            glUniform3f(glGetUniformLocation(shader, "viewPos"), camera_position.x, camera_position.y, camera_position.z);
            glUniform1f(glGetUniformLocation(shader, "material.ambient"), default_material.ambient);
            glUniform1f(glGetUniformLocation(shader, "material.diffuse"), default_material.diffuse);
            glUniform1f(glGetUniformLocation(shader, "material.specular"), default_material.specular);
            glUniform1f(glGetUniformLocation(shader, "material.shininess"), default_material.shininess);
            glUniform3f(glGetUniformLocation(shader, "light.position"), default_light.position.x, default_light.position.y, default_light.position.z);
            glUniform1f(glGetUniformLocation(shader, "light.ambient"), default_light.ambient);
            glUniform1f(glGetUniformLocation(shader, "light.diffuse"), default_light.diffuse);
            glUniform1f(glGetUniformLocation(shader, "light.specular"), default_light.specular);
        }

        for (Node *child: node->children) {
            render(child, model_matrix);
        }
    }

private:
    void supprimerNodeDeParent(Node *parentNode, Node *nodeToRemove) {
        auto it = std::find(parentNode->children.begin(), parentNode->children.end(), nodeToRemove);
        if (it != parentNode->children.end()) {
            parentNode->children.erase(it);
            delete nodeToRemove;
        } else {
            for (Node *child: parentNode->children) {
                supprimerNodeDeParent(child, nodeToRemove);
            }
        }
    }
};
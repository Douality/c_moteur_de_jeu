//Phong
struct Material{
    float diffuse;
    float ambient = 0.1f;
    float specular;
    float shininess = 1.0f;
    Material(){
        diffuse = 1.2f;
        ambient = 1.1f;
        specular = 1.3f;
        shininess = 1.0f;
    }
};

struct Light {
    vec3 position;
    float ambient;
    float diffuse;
    float specular;
    vec3 lightColor;
    Light(){
        position = glm::vec3(2.0f, 2.0f, 2.0f);
        lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
        diffuse = 1.3f;
        specular = 1.3f;
        ambient = 1.1f;
    }
};


//Structure pour controler zone de dpl
struct Mur_invisible{
    glm::vec3 min;
    glm::vec3 max;
    Mur_invisible(){
        min = glm::vec3(-FLT_MAX);
        max = glm::vec3(FLT_MAX);
    }
    Mur_invisible(glm::vec3 mini, glm::vec3 maxi){
        min = mini;
        max = maxi;
    }
};

//Structure pour zone body player
struct Character {
    glm::vec3 haut;
    glm::vec3 mid;
    glm::vec3 bas;
    glm::vec3 lim_basse;
    glm::vec3 lim_haute;
};

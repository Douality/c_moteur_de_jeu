#version 330 core

// Ouput data
in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec2 o_uv0;
out vec4 FragColor;

in float altitude;
in vec3 FragPos;
in vec3 normal;

uniform sampler2D colorTexture;
uniform sampler2D grass;
uniform sampler2D rock;
uniform sampler2D snow;
uniform sampler2D water;
uniform sampler2D skybox;
uniform sampler2D star;
uniform sampler2D end;

uniform vec3 barColor;
uniform int isHealthBar;
uniform int isPoulePart;
uniform int projectile;
uniform int end_game;
uniform int active_phong;
uniform float rayon_x;
uniform float rayon_y;
uniform float rayon_z;

struct Material {
    float ambient;
    float diffuse;
    float specular;
    float shininess;
};

struct Light {
    vec3 position;
    float ambient;
    float diffuse;
    float specular;
};

struct Character{
    vec3 haut;
    vec3 mid;
    vec3 bas;
    vec3 lim_basse;
    vec3 lim_haute;
};

uniform vec3 viewPos;
uniform Material material;
uniform Light light;
uniform Character character;

void main(){

    vec4 grass_texture = texture(grass, o_uv0);
    vec4 rock_texture = texture(rock, o_uv0);
    vec4 texel3 = texture(colorTexture, o_uv0);
    vec4 water_texel = texture(water, o_uv0);
    vec4 skybox_texel = texture(skybox, o_uv0);
    vec4 star_texel = texture(star, o_uv0);
    vec4 endTexture = texture(end, o_uv0);

    //Skybox
    if (o_positionWorld.x < -2.0f || o_positionWorld.y <= -12.0f || o_positionWorld.z <= -12.0f
    ||
    o_positionWorld.x >= 9.0f || o_positionWorld.y >= 7.0f || o_positionWorld.z >= 7.0f)
    {
        FragColor = skybox_texel;
    }
    else if(o_positionWorld.x > character.lim_basse.x && o_positionWorld.x < character.mid.x
    && o_positionWorld.y > character.lim_basse.y && o_positionWorld.y < character.mid.y
    && o_positionWorld.z > character.lim_basse.z && o_positionWorld.z < character.mid.z){
        FragColor = vec4(0.5f,0.5f,0.5f,1.0f);
    }
    // FragColor pour le sol (hauteur à 0 et profondeur > -4)
    else if (o_positionWorld.y == 0.0f && o_positionWorld.z >= -4.0f) {
        FragColor = grass_texture;
    }
    // FragColor pour le mur (hauteur > 0 et profondeur à -4)
    else if (o_positionWorld.y > 0.0f && o_positionWorld.z == -4.0f) {
        FragColor = rock_texture;
    }
    // Water
    else if (o_positionWorld.y < 0.0f && (o_positionWorld.y > -0.1f
    || (o_positionWorld.x > 8.0f && o_positionWorld.x < 8.1f)
    || (o_positionWorld.x > -1.1f && o_positionWorld.x < -0.9f))) {
        FragColor = water_texel;
    }
    else if (o_positionWorld.y > 5.0f && o_positionWorld.x > 3.35f && o_positionWorld.x < 3.9f)
    {
        FragColor = skybox_texel;
    }
    // FragColor pour le sol (hauteur à 0 et profondeur > -4)
    else if (o_positionWorld.y == 0.0f && o_positionWorld.z > -4.0f) {
        FragColor = grass_texture;
        if(projectile != 0) FragColor = skybox_texel;
    }
    // FragColor pour le mur (hauteur > 0 et profondeur à -4)
    else if (o_positionWorld.y > 0.0f && o_positionWorld.z == -4.0f) {
        //FragColor = rock_texture;
    }
    else if (isHealthBar == 1) {
        // Determine which face we are on based on the UV coordinates
        if(o_uv0.x < barColor.x) {
            FragColor = vec4(0, 1.0, 0, 1.0);
        } else {
            FragColor = vec4(1.0, 0, 0, 1.0);
        }
    }

    else if(isPoulePart == 1 || isPoulePart == 2 || isPoulePart == 4 || isPoulePart == 5){
        FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
    else if(isPoulePart == 7 || isPoulePart == 6 || isPoulePart == 3){
        FragColor = vec4(1.0, 0.8, 0.0, 1.0);
    }
    else if(isPoulePart > 7){
        FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
    // FragColor pour le cube (sinon)
    else {
        FragColor = texel3;
    }

    if( active_phong == 1 && isHealthBar != 1){
        vec3 ambient = material.ambient * light.ambient * vec3(FragColor);

        vec3 norm = normalize(normal);
        vec3 lightDir = normalize(light.position - vec3(FragPos));
        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuse = material.diffuse * light.diffuse * diff * vec3(FragColor);

        vec3 viewDir = normalize(viewPos - vec3(FragPos));
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = material.specular * light.specular * spec * vec3(FragColor);

        vec3 result = ambient + diffuse + specular;
        FragColor = vec4(result,1.0f);
    }

    // Un effet pour la fin du jeu ( à revoir )
    if(end_game == 2 
    && (o_positionWorld.x > rayon_x - 0.3f && o_positionWorld.x < rayon_x + 0.4f)
    && (o_positionWorld.y > rayon_z - 0.2f)
    && (o_positionWorld.z > rayon_z - 0.2f && o_positionWorld.z < rayon_z + 0.8f)){
        FragColor = skybox_texel;
    }
}
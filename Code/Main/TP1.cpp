#include "./Entete/Libraries.h"

#include "./Entete/Donnes.h"

/**
 * classe transform qui sera appliqué lors de la création des noeuds
 */
#include "./Class/Transform.h"

/**
 * structure boite englobante alignée sur les axes
 */
#include "./Class/AABB.h"


/*******************************************************************************/

#include "./Fonctions/fct_mesh.h"

/**
 * noeud du graphe de scène
 */
#include "./Class/Node.h"
std::vector<Node*> items = std::vector<Node*>();

/**
 * le graphe de scène
 */
#include "./Class/GrapheScene.h"

/*
Grille de partitionnement
*/
#include "./Class/Grid.h"
Cellule* water_cell;

//Musique
#include "./Class/Musique.h"

//vecteur de cubes lancés
std::vector<Node *> cubesLances;
CubeProperties *cubeProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.015f, glm::vec3(0.0f, 0.0f, 0.0f));

// création de la node root, de la surface plane et du mur
Node root(
        Transform(glm::vec3(1.0f), glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(0.0f, 0.0f, 0.0f)), false, false, false);
Node *surface = new Node(
        Transform(glm::vec3(1.0f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))) *
                  glm::mat3x3(glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f))),
                  glm::vec3(-1.0f, 0.0f, -4.0f)),
        false, true, false, false, glm::vec3(0.0f, 1.0f, 0.0f));

glm::mat4 wall_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
glm::mat4 wall_scale = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f));
glm::mat4 wall_transform = wall_rotation * wall_scale;
Node *mur = new Node(
        Transform(glm::vec3(1.0f), glm::mat3x3(wall_transform), glm::vec3(-1.0f, 4.0f, -4.0f)),
        false, true, false, false, glm::vec3(0.0f, 0.0f, -1.0f));

// Création des pattes de la poule
Node *poulePatte1 = new Node(
        Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(-0.01f, -0.07f, 0.0f)), false, false, true, false, 0.05f, cubeProps);
Node *poulePatte2 = new Node(
        Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(0.01f, -0.07f, 0.0f)), false, false, true, false ,0.05f, cubeProps);
                  
CubeProperties *pouleBodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));
Node* pouleBody2 = new Node(
        Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(1.0f, 0.2f, 0.0f)), false, false, true, false, 0.1f, pouleBodyProps);

#include "./Fonctions/Boss.h"
#include "./Fonctions/Moves.h"

int main(void) {
    
    //Base musique
    init_musique();
    
    #include "./Entete/Init_GLFW.h"

    // Get texture
    #include "./Entete/Map_texture.h"

    // Création mesh
    #include "./Entete/Creation_entities.h"

    //Graphe de scène
    #include "./Entete/Graphe_init.h"

    #include "./Entete/Grille_init.h"

    restart_position = protection->transform.t;
    play_music("./Cybeast.mp3");

    do {
        
        #include "./Entete/Init_buffer.h"

        glm::mat4 model_Matrix;
        model_Matrix = glm::mat4(1.0f);
        glm::mat4 ViewMatrix;
        ViewMatrix = mat4(1.0f);
        ViewMatrix = glm::lookAt(camera_position, camera_target, camera_up);
        glm::mat4 ProjectionMatrix;
        ProjectionMatrix = mat4(1.0f);
        ProjectionMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);

        // Update arm rotation
        updateArmRotation(bras1, bras2, arm_rotation);

        //position du cube en fonction de sa vitesse
        for (Node *cubeLance: cubesLances) {
            updatePosCube(*cubeLance, deltaTime, grapheScene.platforms);
        }

        //camera_target = body->transform.t + glm::vec3(0.0f, 0.0f, -1.0f);
        updateCameraPos(body->transform);

        if (bossIsKilled){
            if(!new_music_play){
                new_music_play = true;
                stop_music();
                play_music("./observatory.mp3");
                last_pos = camera_position;
                target = (pouleBody2->transform.t + glm::vec3(0.0f,0.4f,0.05f));
                pas = glm::vec3((target - Star->transform.t)/(float)nb_frame);
                if(target.x > Star->transform.t.x) direction_star_x = true;
                if(target.y > Star->transform.t.y) direction_star_y = true;
                if(target.z > Star->transform.t.z) direction_star_z = true;
                grapheScene.ajoutPlateforme(Star);
                isThereObjectif = false;
            }
            camera_position = glm::vec3(Star->transform.t) + glm::vec3(3.5f,3.0f,3.0f);
            if(direction_star_x == true ? (Star->transform.t.x + pas.x < target.x) : (Star->transform.t.x + pas.x > target.x)) Star->transform.t.x += pas.x;
            if(direction_star_y == true ? (Star->transform.t.y + pas.y < target.y) : (Star->transform.t.y + pas.y > target.y)) Star->transform.t.y += pas.y;
            if(direction_star_z == true ? (Star->transform.t.z + pas.z < target.z) : (Star->transform.t.z + pas.z > target.z)) Star->transform.t.z += pas.z;
            
            if((direction_star_x == true ? (Star->transform.t.x >= target.x - 0.2f) : (Star->transform.t.x <= target.x + 0.2f))
               &&
               (direction_star_y == true ? (Star->transform.t.y >= target.y - 0.2f) : (Star->transform.t.y <= target.y + 0.2f))
               &&
               (direction_star_z == true ? (Star->transform.t.z >= target.z - 0.2f) : (Star->transform.t.z <= target.z + 0.2f))){
                std::this_thread::sleep_for(std::chrono::seconds(1));
                currentFrame = 0;
                camera_position = last_pos;
                bossIsKilled = false;
                isThereObjectif = true;
            }    
        }

        //affichage node
        //affichage(&root, model_Matrix, programID);
        grapheScene.update(ViewMatrix, ProjectionMatrix);
        grapheScene.render(grapheScene.root, model_Matrix);

        if(isThereObjectif){
            body->cubeProperties->velocity += body->cubeProperties->acceleration * deltaTime;
            glm::vec3 posBody = body->transform.t + body->cubeProperties->velocity * deltaTime;
            float half = body->cubeSize / 2.0f;
            float legHeight = 0.08f;
            bool parTerre = posBody.y - half - legHeight < 0.0f;

            if (parTerre) {
                posBody.y = half + legHeight;
            }

            body->transform.t = posBody;
            body->aabb.min = body->transform.t - glm::vec3(body->cubeSize/2.0f);
            body->aabb.max = body->transform.t + glm::vec3(body->cubeSize/2.0f);

            if((Star->transform.t.x > body->aabb.min.x - 0.5f && Star->transform.t.x < body->aabb.max.x + 0.5f)
                &&
                (Star->transform.t.y > body->aabb.min.y - 0.5f && Star->transform.t.y < body->aabb.max.y + 0.5f)
                &&
                (Star->transform.t.z > body->aabb.min.z - 0.5f && Star->transform.t.z < body->aabb.max.z + 0.5f)){
                if(!asSwitch_music){
                    stop_music();
                    if(my_music == 0){
                        play_music("./Promise.mp3",volume-3);
                    }
                    if(my_music == 1){
                        play_music("./observatory.mp3");
                    }
                    play_music("./Promise.mp3",volume-3);
                    asSwitch_music = true;
                    my_music = (my_music >= 1 ? 0 : my_music+1);
                }
                glUniform1i(glGetUniformLocation(programID, "end_game"), 1);
            }

            glUniform1i(glGetUniformLocation(programID, "end_game"), 2);             
            glUniform1f(glGetUniformLocation(programID, "rayon_x"), Star->transform.t.x);
            glUniform1f(glGetUniformLocation(programID, "rayon_y"), Star->transform.t.y); 
            glUniform1f(glGetUniformLocation(programID, "rayon_z"), Star->transform.t.z); 
        }

        if(active_restart && !isThereObjectif){
            HpPoule = 100;
            std::this_thread::sleep_for(std::chrono::seconds(1));
            player_hp = 20;
            defeat_music = active_restart = false;
            body->transform.t = restart_position;
            active_map_dgt = 0;
            play_music("./battle.mp3");
        }

        //Grille update
        Grid.grid_aabb.min = surface->aabb.min;
        Grid.grid_aabb.max = surface->aabb.max;

        character.lim_basse = body->transform.t;
        character.lim_haute = body->transform.t + glm::vec3(0.2f,0.2f,0.2f);
        character.bas = glm::vec3(0.0f);
        character.mid = glm::vec3(0.25f);
        character.haut = glm::vec3(0.5f);
        GLuint characterLocation = glGetUniformLocation(programID, "character");
        glUniform3fv(characterLocation, 1, glm::value_ptr(character.haut));
        glUniform3fv(characterLocation + 1, 1, glm::value_ptr(character.mid));
        glUniform3fv(characterLocation + 2, 1, glm::value_ptr(character.bas));
        glUniform3fv(characterLocation + 3, 1, glm::value_ptr(character.lim_basse));
        glUniform3fv(characterLocation + 4, 1, glm::value_ptr(character.lim_haute));

        glUniform1i(glGetUniformLocation(programID, "projectile"),(active_map_dgt ? 1 : 0));

        #include "./Entete/GLSL.h"


    } // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 && isPlaying);

    //Clean Musique
    if( isPlaying ) Mix_HaltMusic();
    if( current_music != NULL ) Mix_FreeMusic(current_music);

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &uvbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
#include "./Entete/Inputs.h"

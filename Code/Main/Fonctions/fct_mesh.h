#pragma once

struct CubeProperties {
    glm::vec3 velocity;
    float weight;
    glm::vec3 acceleration;

    CubeProperties(glm::vec3 velocity, float weight, glm::vec3 acceleration)
            : velocity(velocity), weight(weight), acceleration(acceleration) {}
};

void aabb_update(AABB &aabb, const glm::vec3 &point) {
    aabb.min.x = std::min(aabb.min.x, point.x);
    aabb.min.y = std::min(aabb.min.y, point.y);
    aabb.min.z = std::min(aabb.min.z, point.z);
    aabb.max.x = std::max(aabb.max.x, point.x);
    aabb.max.y = std::max(aabb.max.y, point.y);
    aabb.max.z = std::max(aabb.max.z, point.z);
}

void creuse(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax, float profondeur,
            std::vector <glm::vec3> &indexed_vertices) {
    for (int i = 0; i < indexed_vertices.size(); i++) {
        glm::vec3 vertex = indexed_vertices[i];
        if (vertex.x >= xmin && vertex.x <= xmax &&
            vertex.y >= ymin && vertex.y <= ymax &&
            vertex.z >= zmin && vertex.z <= zmax) {
            indexed_vertices[i] = glm::vec3(vertex.x, vertex.y - profondeur, vertex.z);
        }
    }
}

void calculatenormals(std::vector <std::vector<unsigned short>> &triangles,
                      std::vector <glm::vec3> &indexed_vertices,
                      std::vector <glm::vec3> &normals) {
    normals.resize(indexed_vertices.size());
    for (glm::vec3 &normal: normals) {
        normal = glm::vec3(0.0f);
    }

    for (const std::vector<unsigned short> &triangle: triangles) {
        const glm::vec3 v1 = indexed_vertices[triangle[0]];
        const glm::vec3 v2 = indexed_vertices[triangle[1]];
        const glm::vec3 v3 = indexed_vertices[triangle[2]];

        glm::vec3 faceNormal = glm::normalize(glm::cross(v2 - v1, v3 - v1));

        normals[triangle[0]] += faceNormal;
        normals[triangle[1]] += faceNormal;
        normals[triangle[2]] += faceNormal;
    }


    for (glm::vec3 &normal: normals) {
        normal = glm::normalize(normal);
    }
}

/**
 * fonction de création d'une surface
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param resolution
 * @param detailFactor paramètre utilisé pour le LOD
 */
void surfacePlane(std::vector<unsigned short> &indices,
                  std::vector <std::vector<unsigned short>> &triangles,
                  std::vector <glm::vec3> &indexed_vertices,
                  std::vector <glm::vec2> &uvs,
                  std::vector <glm::vec3> &normals,
                  float resolution,
                  float detailFactor,
                  bool heightmap = false) {

    double size = 1.0; //unused for now
    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    resolution = resolution * detailFactor;

    //création sommets
    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            indexed_vertices.push_back(
                    glm::vec3((float) i / (resolution - 1), 0, (float) j / (resolution - 1)));
        }
    }

    //création uvs
    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            uvs.push_back(glm::vec2((float) i / (resolution - 1), (float) j / (resolution - 1)));
        }
    }

    //création triangles
    std::vector<unsigned short> triangleFactory(3);
    for (int i = 0; i < resolution - 1; i++) {
        for (int j = 0; j < resolution - 1; j++) {
            indices.push_back(i * resolution + j);
            indices.push_back(i * resolution + (j + 1));
            indices.push_back((i + 1) * resolution + j);
            triangleFactory[0] = i * resolution + j;
            triangleFactory[1] = i * resolution + (j + 1);
            triangleFactory[2] = (i + 1) * resolution + j;
            triangles.push_back(triangleFactory);
            indices.push_back((i + 1) * resolution + j);
            indices.push_back(i * resolution + (j + 1));
            indices.push_back((i + 1) * resolution + (j + 1));
            triangleFactory[0] = (i + 1) * resolution + j;
            triangleFactory[1] = i * resolution + (j + 1);
            triangleFactory[2] = (i + 1) * resolution + (j + 1);
            triangles.push_back(triangleFactory);
        }
    }

    normals.resize(indexed_vertices.size(), glm::vec3(0.0f));

    for (const auto &triangle: triangles) {
        const glm::vec3 &v1 = indexed_vertices[triangle[0]];
        const glm::vec3 &v2 = indexed_vertices[triangle[1]];
        const glm::vec3 &v3 = indexed_vertices[triangle[2]];

        const glm::vec3 faceNormal = glm::normalize(glm::cross(v2 - v1, v3 - v1));

        normals[triangle[0]] += faceNormal;
        normals[triangle[1]] += faceNormal;
        normals[triangle[2]] += faceNormal;
    }

    for (auto &normal: normals) {
        normal = glm::normalize(normal);
    }
}

/**
 * fonction de création d'une sphère
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param resolution
 * @param size taille de la sphère
 * @param detailFactor paramètre utilisé pour le LOD
 */
void newSphere(std::vector<unsigned short> &indices,
               std::vector <std::vector<unsigned short>> &triangles,
               std::vector <glm::vec3> &indexed_vertices,
               std::vector <glm::vec2> &uvs,
               std::vector <glm::vec3> &normals,
               float resolution, float size,
               float detailFactor) {

    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    resolution = resolution * detailFactor;

    int num_vertices = (resolution + 1) * (resolution + 1);
    indexed_vertices.reserve(num_vertices);

    float pi = glm::pi<float>();
    float theta_step = 2.0f * pi / resolution;
    float phi_step = pi / resolution;

    for (int i = 0; i <= resolution; i++) {
        float theta = i * theta_step;

        for (int j = 0; j <= resolution; j++) {
            float phi = j * phi_step;
            float x = sin(phi) * cos(theta);
            float y = sin(phi) * sin(theta);
            float z = cos(phi);
            indexed_vertices.push_back(glm::vec3(x * size, y * size, z * size));

            glm::vec2 uv((float) i / resolution, (float) j / resolution);
            uvs.push_back(uv);
        }
    }

    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            short unsigned int a = i * (resolution + 1) + j;
            short unsigned int b = (i + 1) * (resolution + 1) + j;
            short unsigned int c = (i + 1) * (resolution + 1) + j + 1;
            short unsigned int d = i * (resolution + 1) + j + 1;
            triangles.push_back({a, b, d});
            triangles.push_back({b, c, d});
        }
    }

    // On met à jour les indices
    for (const auto &triangle: triangles) {
        for (int i = 0; i < 3; i++) {
            indices.push_back(triangle[i]);
        }
    }

    normals.resize(indexed_vertices.size(), glm::vec3(0.0f));

    for (const auto &triangle: triangles) {
        const glm::vec3 &v1 = indexed_vertices[triangle[0]];
        const glm::vec3 &v2 = indexed_vertices[triangle[1]];
        const glm::vec3 &v3 = indexed_vertices[triangle[2]];

        const glm::vec3 faceNormal = glm::normalize(glm::cross(v2 - v1, v3 - v1));

        normals[triangle[0]] += faceNormal;
        normals[triangle[1]] += faceNormal;
        normals[triangle[2]] += faceNormal;
    }

    for (auto &normal: normals) {
        normal = glm::normalize(normal);
    }
}

/**
 * fonction de création d'un cube
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param size taille du cube
 * @param detailFactor paramètre utilisé pour le LOD
 */
void newCube(std::vector<unsigned short> &indices,
             std::vector <std::vector<unsigned short>> &triangles,
             std::vector <glm::vec3> &indexed_vertices,
             std::vector <glm::vec2> &uvs,
             std::vector <glm::vec3> &normals,
             float size, float detailFactor, CubeProperties *cubeProperties) {
    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    int num_vertices = 8;
    indexed_vertices.reserve(num_vertices);

    // Coordonnées des sommets du cube
    glm::vec3 vertices[6][4] = {
            {{-0.5f, -0.5f, 0.5f},  {0.5f,  -0.5f, 0.5f},  {0.5f,  0.5f,  0.5f},  {-0.5f, 0.5f,  0.5f}},
            {{0.5f,  -0.5f, 0.5f},  {0.5f,  -0.5f, -0.5f}, {0.5f,  0.5f,  -0.5f}, {0.5f,  0.5f,  0.5f}},
            {{0.5f,  -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}, {-0.5f, 0.5f,  -0.5f}, {0.5f,  0.5f,  -0.5f}},
            {{-0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, 0.5f},  {-0.5f, 0.5f,  0.5f},  {-0.5f, 0.5f,  -0.5f}},
            {{-0.5f, 0.5f,  0.5f},  {0.5f,  0.5f,  0.5f},  {0.5f,  0.5f,  -0.5f}, {-0.5f, 0.5f,  -0.5f}},
            {{-0.5f, -0.5f, -0.5f}, {0.5f,  -0.5f, -0.5f}, {0.5f,  -0.5f, 0.5f},  {-0.5f, -0.5f, 0.5f}}
    };

    // Coordonnées de texture pour chaque face
    glm::vec2 faceUVs[6][4] = {
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face avant
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face droite
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face arrière
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face gauche
            {{0.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 0.0f}}, // Face du dessus
            {{0.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 0.0f}}  // Face du dessous
    };


    // Ajouter les sommets et les coordonnées de texture pour chaque face à indexed_vertices et uvs
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 4; j++) {
            indexed_vertices.push_back(vertices[i][j] * size);
            uvs.push_back(faceUVs[i][j]);
        }
    }

    // Indices des triangles formant les faces du cube
    unsigned short faceIndices[6][6] = {
            {0,  1,  2,  2,  3,  0},
            {4,  5,  6,  6,  7,  4},
            {8,  9,  10, 10, 11, 8},
            {12, 13, 14, 14, 15, 12},
            {16, 17, 18, 18, 19, 16},
            {20, 21, 22, 22, 23, 20}

    };

    // Ajouter les indices des triangles aux tableaux 'triangles' et 'indices'
    for (int i = 0; i < 6; i++) {
        std::vector<unsigned short> triangle1 = {faceIndices[i][0], faceIndices[i][1], faceIndices[i][2]};
        std::vector<unsigned short> triangle2 = {faceIndices[i][3], faceIndices[i][4], faceIndices[i][5]};
        triangles.push_back(triangle1);
        triangles.push_back(triangle2);
    }

    // Mettre à jour les indices
    for (const auto &triangle: triangles) {
        for (int i = 0; i < 3; i++) {
            indices.push_back(triangle[i]);
        }
    }

    normals.resize(indexed_vertices.size(), glm::vec3(0.0f));

    for (const auto &triangle: triangles) {
        const glm::vec3 &v1 = indexed_vertices[triangle[0]];
        const glm::vec3 &v2 = indexed_vertices[triangle[1]];
        const glm::vec3 &v3 = indexed_vertices[triangle[2]];

        const glm::vec3 faceNormal = glm::normalize(glm::cross(v2 - v1, v3 - v1));

        normals[triangle[0]] += faceNormal;
        normals[triangle[1]] += faceNormal;
        normals[triangle[2]] += faceNormal;
    }

    for (auto &normal: normals) {
        normal = glm::normalize(normal);
    }
}

void Arbre(std::vector<unsigned short> &indices,
           std::vector <std::vector<unsigned short>> &triangles,
           std::vector <glm::vec3> &indexed_vertices,
           std::vector <glm::vec2> &uvs,
           std::vector <glm::vec3> &normals,
           float hauteur,
           const glm::vec3 position,
           AABB &aabb) {

    float rayon_tronc = hauteur / 10.0f;
    float rayon_feuilles = hauteur / 2.0f;

    // Génération du tronc
    int nb_meridiens = 16;
    int nb_paralleles = 10;
    float delta_theta = 2.0f * glm::pi<float>() / nb_meridiens;
    float delta_phi = glm::pi<float>() / nb_paralleles;

    int indice_base_tronc = indexed_vertices.size();
    for (int j = 0; j <= nb_paralleles; j++) {
        for (int i = 0; i <= nb_meridiens; i++) {
            float x = rayon_tronc * glm::sin(delta_phi * j) * glm::cos(delta_theta * i);
            float y = hauteur / 2.0f + rayon_tronc * glm::cos(delta_phi * j);
            float z = rayon_tronc * glm::sin(delta_phi * j) * glm::sin(delta_theta * i);
            indexed_vertices.push_back(glm::vec3(x, y, z));
            uvs.push_back(glm::vec2(float(i) / nb_meridiens, float(j) / nb_paralleles));
        }
    }
    for (int j = 0; j < nb_paralleles; j++) {
        for (int i = 0; i < nb_meridiens; i++) {
            short unsigned int p1 = j * (nb_meridiens + 1) + i;
            short unsigned int p2 = (j + 1) * (nb_meridiens + 1) + i;
            short unsigned int p3 = j * (nb_meridiens + 1) + i + 1;
            short unsigned int p4 = (j + 1) * (nb_meridiens + 1) + i + 1;
            triangles.push_back({p1, p2, p3});
            triangles.push_back({p4, p3, p2});
            indices.push_back(p1 + indice_base_tronc);
            indices.push_back(p2 + indice_base_tronc);
            indices.push_back(p3 + indice_base_tronc);
            indices.push_back(p4 + indice_base_tronc);
            indices.push_back(p3 + indice_base_tronc);
            indices.push_back(p2 + indice_base_tronc);
        }
    }

    // Génération des feuilles
    nb_meridiens = 8;
    nb_paralleles = 8;
    delta_theta = 2.0f * glm::pi<float>() / nb_meridiens;
    delta_phi = glm::pi<float>() / nb_paralleles;
    for (int j = 0; j <= nb_paralleles; j++) {
        for (int i = 0; i <= nb_meridiens; i++) {
            float x = rayon_feuilles * cos(2 * M_PI * i / nb_meridiens) * sin(M_PI * j / nb_paralleles);
            float y = hauteur * cos(M_PI * j / nb_paralleles);
            float z = rayon_feuilles * sin(2 * M_PI * i / nb_meridiens) * sin(M_PI * j / nb_paralleles);
            glm::vec3 vertex = glm::vec3(x, y, z) + position;
            indexed_vertices.push_back(vertex);

            float u = (float) i / nb_meridiens;
            float v = (float) j / nb_paralleles;

            uvs.push_back(glm::vec2(u, v));
        }
    }

    for (int j = 0; j < nb_paralleles; j++) {
        for (int i = 0; i < nb_meridiens; i++) {
            int p1 = j * (nb_meridiens + 1) + i;
            int p2 = p1 + (nb_meridiens + 1);

            triangles[j].push_back(p1);
            triangles[j].push_back(p2);
            triangles[j].push_back(p1 + 1);

            triangles[j].push_back(p1 + 1);
            triangles[j].push_back(p2);
            triangles[j].push_back(p2 + 1);

            indices.push_back(p1);
            indices.push_back(p2);
            indices.push_back(p1 + 1);
            indices.push_back(p1 + 1);
            indices.push_back(p2);
            indices.push_back(p2 + 1);
        }
    }

    // Calcul de la boîte englobante (AABB)
    glm::vec3 minValues = indexed_vertices[0];
    glm::vec3 maxValues = indexed_vertices[0];

    for (int i = 1; i < indexed_vertices.size(); i++) {
        minValues = glm::min(minValues, indexed_vertices[i]);
        maxValues = glm::max(maxValues, indexed_vertices[i]);
    }

    aabb.min = minValues;
    aabb.max = maxValues;

    normals.resize(indexed_vertices.size(), glm::vec3(0.0f));

    for (const auto &triangle: triangles) {
        const glm::vec3 &v1 = indexed_vertices[triangle[0]];
        const glm::vec3 &v2 = indexed_vertices[triangle[1]];
        const glm::vec3 &v3 = indexed_vertices[triangle[2]];

        const glm::vec3 faceNormal = glm::normalize(glm::cross(v2 - v1, v3 - v1));

        normals[triangle[0]] += faceNormal;
        normals[triangle[1]] += faceNormal;
        normals[triangle[2]] += faceNormal;
    }

    for (auto &normal: normals) {
        normal = glm::normalize(normal);
    }
}
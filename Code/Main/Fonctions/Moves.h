#pragma once

/**
 * fonction pour calculer le rebond du cube sur une surface
 * @param cube
 * @param normal normale de la surface
 */
void resolveCollision(Node &cube, glm::vec3 &normal, float restitution) {
    // Coefficient de restitution (élasticité) pour déterminer la vitesse de sortie.
    // 0 = inelastique, 1 = prfaitement élastique (pas de perte d'énergie cinétique)

    // Calculer la vitesse de sortie en utilisant la formule de réflexion
    cube.cubeProperties->velocity = cube.cubeProperties->velocity -
                                    (1 + restitution) * glm::dot(cube.cubeProperties->velocity, normal) * normal;
}

/**
 * fonction pour modifier la  position du cube en fonction de sa vitesse
 * cette fonction utilise la structure AABB
 * @param cube
 * @param deltaTime
 */
void updatePosCube(Node &cube, float deltaTime, std::vector<Node *> &platforms) {
    cube.cubeProperties->velocity += cube.cubeProperties->acceleration * deltaTime;
    glm::vec3 posCube = cube.transform.t + cube.cubeProperties->velocity * deltaTime;
    float half = cube.cubeSize / 2.0f;
    float mur_half_width = 5.0f; // Largeur du mur divisée par 2
    bool parTerre = posCube.y - half < 0.0f;

    cube.transform.t = posCube;
    cube.aabb.min = cube.transform.t - glm::vec3(cube.cubeSize / 2.0f);
    cube.aabb.max = cube.transform.t + glm::vec3(cube.cubeSize / 2.0f);

    for (Node *platform: platforms) {
        if (checkAABBCollision(cube.aabb, platform->aabb)) {
            if (platform->isSurface) {
                if (platform->surfaceOrientation == glm::vec3(0.0f, 1.0f, 0.0f)) {
                    posCube.y = platform->aabb.max.y + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 1.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(1.0f, 0.0f, 0.0f)) {
                    posCube.x = platform->aabb.min.x - cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(1.0f, 0.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(-1.0f, 0.0f, 0.0f)) {
                    posCube.x = platform->aabb.max.x + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(-1.0f, 0.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, 1.0f)) {
                    posCube.z = platform->aabb.max.z + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, 1.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, -1.0f)) {
                    posCube.z = platform->aabb.max.z + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, -1.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                }
            }
        }
    }


    // Le coefficient de friction est compris entre 0 et 1,
    // où 0 signifie aucune friction et 1 signifie une friction infinie.
    if (parTerre) {
        float coef_f = 0.1;
        cube.cubeProperties->velocity.x *= (1 - coef_f);
        cube.cubeProperties->velocity.z *= (1 - coef_f);

        //ajout d'une borne inf pour stopper complètement le cube si vitesse trop faible
        float borne_inf = 0.001f;
        if (glm::length(cube.cubeProperties->velocity) < borne_inf) {
            cube.cubeProperties->velocity = glm::vec3(0.0f);
        }
    }

    cube.transform.t = posCube;

    //std::cout << "pos cube: " << cube.transform.t.x << ", " << cube.transform.t.y << ", " << cube.transform.t.z
    //          << std::endl;
}

/**
 * fonction test pour vérifier la rotation des nodes bras
 * @param bras1
 * @param bras2
 * @param arm_rotation
 */
void updateArmRotation(Node *bras1, Node *bras2, float arm_rotation) {
    glm::mat3x3 rotationMatrix = glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f)));
    bras1->transform.r = rotationMatrix;
    bras2->transform.r = rotationMatrix;
}

void faceAFace(Node &body, Node &pouleBody) {
    glm::vec3 direction = glm::normalize(body.transform.t - pouleBody.transform.t);
    float angleBody = atan2(direction.x, direction.z);
    float anglePoule = angleBody + glm::pi<float>();

    float angleSupplementaire = glm::pi<float>(); // Ajustez l'angle supplémentaire pour que le personnage fasse face à la poule avec la face avant de son corps
    body.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), angleBody + angleSupplementaire + glm::pi<float>(),
                                               glm::vec3(0.0f, 1.0f, 0.0f)));
    pouleBody.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), anglePoule, glm::vec3(0.0f, 1.0f, 0.0f)));
}

/**
 * fonction de reposition du body
 * @param body
 * @param deltaTime
 * @param platforms
 */
void updatePosBody(Node &body, Node &pouleBody, float deltaTime, std::vector<Node *> &platforms) {
    AABB light_aabb = AABB(pouleBody2->aabb.min - glm::vec3(0.4f),pouleBody2->aabb.max + glm::vec3(0.4f));
    bool dgt = checkAABBCollision(body.aabb,light_aabb); 
    player_hp -= (dgt? 1 : 0);
    //Pour présentation
    if(print_player_pv) std::cout << "Hidden HP player = " << player_hp << "\n------\n" <<std::endl;
    //
    bool cinematique_fin_touche = (cinematique && checkAABBCollision(body.aabb,pouleBody2->aabb)); 
    if(pouleBody2->active || cinematique){
        if((dgt && !FIN) || cinematique_fin_touche){
            if(!defeat_music){
                play_music("./ultraman.mp3",volume,0);
                defeat_music = true;
            }
            if(cinematique_fin_touche) jump_max = 100000;
            body.transform.t.y += 5.0f * deltaTime;
            body.aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            body.aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            pouleBody2->transform.t.y = body.transform.t.y;
            pouleBody2->aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            pouleBody2->aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            camera_target = body.transform.t;
            camera_position = glm::vec3(0.0f, 1.0f, 3.0f);
            HpPoule = 100;
            player_hp = 20;
            if(body.transform.t.y >= 10.f){
                defeat_music = false;
                body.transform.t = restart_position;
                if(!cinematique_fin_touche) play_music("./battle.mp3");
                else play_music("./obervatory.mp3");
            }
        }
    }

    body.cubeProperties->velocity += body.cubeProperties->acceleration * deltaTime;
    glm::vec3 posBody = body.transform.t + body.cubeProperties->velocity * deltaTime;
    float half = body.cubeSize / 2.0f;

    // Prendre en compte la hauteur des jambes pour le contact avec le sol
    float legHeight = 0.08f;
    Cellule * cell = Grid.find_cell(&body);
    bool parTerre;
    inWater = (cell == water_cell);
    inWater = false;
    if (!inWater) {
        for(auto c : Grid.elem){
            if((Cellule*)c!=NULL) inWater = (cell == (Cellule*)c);
        }
    }
    std::cout << inWater << std::endl;
    parTerre = posBody.y - half - legHeight < (!active_map_dgt ? 0.0f : (inWater ? -0.2f : 0.2f));
    surface->aabb.max.y -= (inWater? 0.02f : 0.0f);
    surface->aabb.min.y -= (inWater? 0.02f : 0.0f);

    pouleBody.cubeProperties->velocity += pouleBody.cubeProperties->acceleration * deltaTime;
    glm::vec3 posPoule = pouleBody.transform.t + pouleBody.cubeProperties->velocity * deltaTime;
    pouleBody.transform.t = posPoule;

    // Créer une variable pour la hauteur des pattes de la poule
    float patteHeight = 0.4f;

    // Vérifiez si la poule est au sol et ajustez sa position en conséquence
    bool pouleOnPlatform = false;

    for (Node *platform: platforms) {
        if (checkAABBCollision(pouleBody.aabb, platform->aabb)) {
            posPoule.y = platform->aabb.max.y + pouleBody.cubeSize / 2.0f + patteHeight;
            pouleBody.cubeProperties->velocity.y = 0.0f;
            pouleOnPlatform = true;
            break;
        }
    }
    if (!pouleOnPlatform && pouleAuSol(&pouleBody)) {
        pouleBody.cubeProperties->velocity.y = 0.0f;
        posPoule.y = pouleBody.cubeSize / 2.0f + patteHeight;
    }
    pouleBody.transform.t = posPoule;


    // Appeler movePoule en passant le paramètre pouleAuSol
    movePoule(pouleBody, body, !pouleOnPlatform && pouleAuSol(&pouleBody));

    if (parTerre) {
        body.cubeProperties->velocity.y = 0.0f;
        float coef_f = 0.1;
        body.cubeProperties->velocity.x *= (1 - coef_f);
        body.cubeProperties->velocity.z *= (1 - coef_f);

        // Ajout d'une borne inf pour stopper complètement le personnage si vitesse trop faible
        float borne_inf = 0.01f;
        if (glm::length(body.cubeProperties->velocity) < borne_inf) {
            body.cubeProperties->velocity = glm::vec3(0.0f);
        }

        // Ajuster la position y de body pour qu'elle repose sur le sol
        posBody.y = half + legHeight;
        if(active_map_dgt){
            player_hp--;
            active_map_dgt = false;
        }
        if(player_hp <= 0){
            active_restart = true;
            if(defeat_music = true){
                defeat_music = false;
                play_music("./battle.mp3");
            }
        }
    }
    posBody.y += (inWater? -0.02f : 0.0f);

    body.transform.t = posBody;
    body.aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
    body.aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;

    for (Node *platform: platforms) {
        if (checkAABBCollision(body.aabb, platform->aabb)) {
            if (platform->isSurface) {
                if (platform->surfaceOrientation == glm::vec3(0.0f, 1.0f, 0.0f)) {
                    posBody.y = platform->aabb.max.y + body.cubeSize / 2.0f + legHeight;
                    glm::vec3 normaleAuPlan(0.0f, 1.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);

                } else if (platform->surfaceOrientation == glm::vec3(1.0f, 0.0f, 0.0f)) {
                    posBody.x = platform->aabb.min.x - body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(1.0f, 0.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(-1.0f, 0.0f, 0.0f)) {
                    posBody.x = platform->aabb.max.x + body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(-1.0f, 0.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, 1.0f)) {
                    posBody.z = platform->aabb.min.z - body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, 1.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, -1.0f)) {
                    posBody.z = platform->aabb.max.z + body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, -1.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;
                }
            }
        }
    }
    body.transform.t = posBody;
    pouleBody.transform.t = posPoule;
    pouleBody.aabb.min = pouleBody.transform.t - glm::vec3(pouleBody.cubeSize / 2.0f) * pouleBody.transform.s;
    pouleBody.aabb.max = pouleBody.transform.t + glm::vec3(pouleBody.cubeSize / 2.0f) * pouleBody.transform.s;

    faceAFace(body, pouleBody);
}

float verticalAng, horizontalAng;
void updateCameraPos(Transform player) {
    camera_target = player.t;
    //
    if(horizontalAng && verticalAng) {
        // Get mouse position
        double xpos, ypos;
        float mouseSpeed = 0.003f;
        glfwGetCursorPos(window, &xpos, &ypos);
        // reset mouse for next frame
        glfwSetCursorPos(window, 1024 / 2, 768 / 2);

        horizontalAng += mouseSpeed * float(1024 / 2 - xpos);
        verticalAng += mouseSpeed * float(768 / 2 - ypos);
        verticalAng = std::clamp(verticalAng, -0.3f, 0.01f);

        glm::vec3 direction(
                cos(verticalAng) * sin(horizontalAng),
                sin(verticalAng),
                cos(verticalAng) * cos(horizontalAng)
        );

        glm::vec3 final_camera_pos = camera_target - direction * 1.0f;
        camera_position = final_camera_pos;
    } else {
        camera_position = player.t + glm::vec3(0, 0.1f, 0.25f);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        // Compute new orientation
        glm::vec3 direction = glm::normalize(camera_target - camera_position);
        glm::vec3 right = glm::normalize(glm::cross(camera_up, direction));
        verticalAng = asin(direction.y);
        horizontalAng = acos(right.z) + 3.14f / 2.0f;
    }
}

float lerpRotation(float x, float y, float t) {

    x = x - ((int)(x/2/3.14f)) * (2 * 3.14f);
    y = y - ((int)(y/2/3.14f)) * (2 * 3.14f);

    if(x - y > 3.14) {
        y += 3.14 * 2;
    } else if(x - y < -3.14) {
        x += 3.14 * 2;
    }

    return x * (1.0 - t) + y * t;
}
float targetRotation, currentRotation;

/*Zone water
        Grid.add_cell(
        Grille[][] -> 0,0,
        Bordures/AABB -> min,max = glm::vec3(-1.0f,0.15f,-3.0f),glm::vec3(7.0f,0.30f,-0.6f),
        coeff -> vitesse, friction, gravity = 0.8f,0.8f,-0.08f);
*/
/**
 * fonction permettant un déplacement simple du body
 * @param body
 * @param window
 */
void moveBody(Node &body, GLFWwindow *window) {
    Cellule * cell = Grid.find_cell(&body);
    float vit = 1.0f;
    if(cell != nullptr) {
        vit *= cell->physics.coeff_vitesse;
        vit *= cell->physics.coeff_friction;
    }

    float bodySpeed = vit * deltaTime;
    glm::vec3 inputDirection = glm::vec3(0,0,0);
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        inputDirection.z -= vit;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        inputDirection.z += vit;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        inputDirection.x -= vit;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        inputDirection.x += vit;
    }
    float rotationY = horizontalAng - 3.14f;
    glm::vec3 direction(
            cos(0) * sin(rotationY),
            sin(0),
            cos(0) * cos(rotationY));

    // Right vector
    glm::vec3 right = glm::vec3(
            sin(rotationY - 3.14 / 2.0f),
            0,
            cos(rotationY - 3.14 / 2.0f));

    glm::vec3 dpl_body = (-inputDirection.x * right + inputDirection.z * direction) * bodySpeed;
    if((body.transform.t.z + dpl_body.z > mur_invisible.min.z)
    && (body.transform.t.z + dpl_body.z < mur_invisible.max.z)){
        body.transform.t.z += dpl_body.z;
    } 
    if((body.transform.t.x + dpl_body.x > mur_invisible.min.x)
    && (body.transform.t.x + dpl_body.x < mur_invisible.max.x)){
        body.transform.t.x += dpl_body.x;
    }

    if(!targetRotation) {
        targetRotation = rotationY;
        currentRotation = rotationY;
    }
    if(inputDirection.x != 0 && inputDirection.z == 0) {
        targetRotation = rotationY - inputDirection.x * 3.14 / 2;
    } else if(inputDirection.x == 0 && inputDirection.z != 0) {
        targetRotation = rotationY + std::clamp(inputDirection.z, 0.0f, 1.0f) * 3.14f;
    } else if(inputDirection.x == 1 && inputDirection.z == 1) {
        targetRotation = rotationY + 3.14 / 4;
    } else if(inputDirection.x == -1 && inputDirection.z == 1) {
        targetRotation = rotationY + 7 * 3.14 / 4;
    } else if(inputDirection.x == 1 && inputDirection.z == -1) {
        targetRotation = rotationY + 5 * 3.14 / 4;
    } else if(inputDirection.x == -1 && inputDirection.z == 1) {
        targetRotation = rotationY + 3 * 3.14 / 4;
    }
    currentRotation = lerpRotation(currentRotation, targetRotation, 0.3f);
    body.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), currentRotation , glm::vec3(0.0f, 1.0f, 0.0f)));

    static bool jumpPressed = false;
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && !jumpPressed && nb_jump < jump_max && (body.transform.t.y <= y_jump_max && ! cinematique)) {
        jumpPressed = true;

        // Utilisez les mêmes paramètres de vitesse et d'accélération que dans jeterCube
        float v0 = 0.8f; // vitesse initiale
        float poids = body.cubeProperties->weight;

        // Ajouter une force vers le haut pour simuler un saut
        body.cubeProperties->velocity.y += v0;

        // Mettre à jour le poids du body et l'accélération due à la gravité
        //body.cubeProperties->weight = poids;
        body.cubeProperties->acceleration = gravity * poids;

        nb_jump++;
        active_map_dgt=(nb_jump!=0&&nb_jump%3==0 ? true : false);
    }
    if(nb_jump >= jump_max && body.transform.t.y <= 0.3f){
        nb_jump = 0;
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
        jumpPressed = false;
    }

    /*std::cout << "body x: " << body.transform.t.x << ", y: " << body.transform.t.y << ", z: " << body.transform.t.z
              << std::endl;*/
}

void jeterCube(Transform &bras1Transform, glm::vec3 upperArmEnd1, GrapheScene &grapheScene, Node* poule, GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && shotFired) {
        if(checkAABBCollision(body->aabb,protection->aabb)) return;
        updateRotations = true;
        shotFired = false;
        float v0 = 0.8f; // vitesse initiale
        float poids = 0.1f;

        // Utiliser l'orientation du bras pour déterminer la direction du lancer
        glm::vec3 forward = glm::normalize(bras1Transform.r * glm::vec3(0.0f, 0.0f, -1.0f));

        glm::vec3 startPosition = bras1Transform.t + bras1Transform.r * upperArmEnd1;

        CubeProperties *cubeProperties = new CubeProperties(v0 * forward, poids, gravity * poids);

        Node *newCubeLance = new Node(
                Transform(glm::vec3(1.0f), glm::mat3x3(1.0f), startPosition),
                false, false, true, false, 0.1f, cubeProperties);
        grapheScene.ajoutNode(newCubeLance);
        cubesLances.push_back(newCubeLance);

    } else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
        shotFired = true; // Mettre à jour l'état de la touche 'M' lorsqu'elle est relâchée
    }
    for (Node *cubeLance : cubesLances) {
        poulePrendDegats(cubeLance, poule, grapheScene, 10);
    }
}
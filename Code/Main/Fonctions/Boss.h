#pragma once

bool pouleAuSol(const Node *pouleBody) {
    Transform identityTransform(glm::vec3(1.0f), glm::mat3x3(1.0f), glm::vec3(0.0f));

    // Parcourir les enfants de pouleBody pour trouver les pattes
    for (const Node *child : pouleBody->children) {
        if (child == poulePatte1 || child == poulePatte2) {
            // Obtenir la transformation absolue de la patte
            Transform absolutePatteTransform = getAbsoluteTransform(const_cast<Node *>(pouleBody), const_cast<Node *>(child), identityTransform);
            float patteHeight = 0.4f;

            // Vérifier si la patte est au sol
            if ((absolutePatteTransform.t.y - child->cubeSize / 2.0f - patteHeight) < (inWater ? -0.2 : 0.0f)) {
                return true;
            }
        }
    }

    return false;
}

void movePoule(Node &pouleBody, const Node &playerBody, bool pouleAuSol) {
    /*std::cout << "poulebody x: " << pouleBody.transform.t.x 
                << ", y: " << pouleBody.transform.t.y 
                << ", z: " << pouleBody.transform.t.z 
                << ", pouleAuSol?: " << pouleAuSol << ", HP:" << HpPoule << std::endl;*/

    if (pouleAuSol) {
        // Utiliser les mêmes paramètres de vitesse et d'accélération que dans moveBody
        float v0 = 0.5f; // vitesse initiale
        float poids = pouleBody.cubeProperties->weight;

        // Calculer la direction du saut en fonction de la position du joueur
        glm::vec3 jumpDirection = glm::normalize(playerBody.transform.t - pouleBody.transform.t);
        jumpDirection.y = 0.0f; // Ignorer la composante y pour le moment
        pouleBody.cubeProperties->velocity = v0 * jumpDirection;

        // Ajouter une force vers le haut pour simuler un saut
        pouleBody.cubeProperties->velocity.y += v0;

        // Mettre à jour le poids de la poule et l'accélération due à la gravité
        pouleBody.cubeProperties->weight = poids;
        pouleBody.cubeProperties->acceleration = (active_map_dgt? dis(gen) * gravity * poids : gravity * poids);
    }
}

/**
 * création de la minipoule à la fin du combat de boss
 * @param grapheScene
 */
void creerPoule(GrapheScene grapheScene){

    pouleBody2->active = true;

    Node *pouleTete2 = new Node(
            Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.05f, -0.07f)), false, false, true, false, 0.05f, cubeProps);

    Node *pouleBec2 = new Node(
            Transform(glm::vec3(0.5f, 0.5f, 1.2f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.0f, -0.035f)), false, false, true, false, 0.02f, cubeProps);

    Node *pouleCrete2 = new Node(
            Transform(glm::vec3(0.2f, 0.65f, 0.4f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.02f, 0.0f)), false, false, true, false, 0.05f, cubeProps);

    Node *pouleAile12 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);

    Node *pouleAile22 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);

    Node *poulePatte12 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.01f, -0.07f, 0.0f)), false, false, true, false, 0.05f, cubeProps);

    Node *poulePatte22 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.01f, -0.07f, 0.0f)), false, false, true, false, 0.05f, cubeProps);

    grapheScene.ajoutNode(pouleBody2);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleTete2);
    grapheScene.ajoutNodeAParent(pouleTete2, pouleBec2);
    grapheScene.ajoutNodeAParent(pouleTete2, pouleCrete2);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleAile12);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleAile22);
    grapheScene.ajoutNodeAParent(pouleBody2, poulePatte12);
    grapheScene.ajoutNodeAParent(pouleBody2, poulePatte22);

    /*std::vector<Node*> pouleParts = {pouleBody2, pouleTete, pouleBec, pouleAile1, pouleAile2, poulePatte1, poulePatte2, pouleCrete};
    grapheScene.pouleParts.assign(pouleParts.begin(), pouleParts.end());*/
    grapheScene.pouleParts.clear();
    std::vector<Node*> pouleParts = {pouleBody2, pouleTete2, pouleBec2, pouleAile12, pouleAile22, poulePatte12, poulePatte22, pouleCrete2};
    for(auto part : pouleParts) {
        grapheScene.pouleParts.push_back(part);
    }

}


void poulePrendDegats(Node* cube, Node* poule, GrapheScene &grapheScene, int damage) {
    if (checkAABBCollision(cube->aabb, poule->aabb)) {
        HpPoule -= damage;
        if (HpPoule <= 0) {
            //HpPoule = 0;
            grapheScene.supprimerNode(poule);
            creerPoule(grapheScene);
            bossIsKilled = true;
            FIN = true;
            cinematique = true;
        }
        // Trouvez le cube dans le vecteur cubesLances et supprimez-le
        auto it = std::find(cubesLances.begin(), cubesLances.end(), cube);
        if (it != cubesLances.end()) {
            grapheScene.supprimerNode(*it);
            cubesLances.erase(it);
        }
    }
}
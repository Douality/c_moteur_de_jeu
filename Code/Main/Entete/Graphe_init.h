#pragma once
    GrapheScene grapheScene(&root, programID);
    grapheScene.ajoutPlateforme(surface);
    grapheScene.ajoutPlateforme(mur);

    grapheScene.ajoutNode(body);
    grapheScene.ajoutNodeAParent(body, tete);
    grapheScene.ajoutNodeAParent(body, jambe1);
    grapheScene.ajoutNodeAParent(body, jambe2);
    grapheScene.ajoutNodeAParent(body, bras1);
    grapheScene.ajoutNodeAParent(body, bras2);

    // Ajout des nœuds centre de rotation à la scène
    grapheScene.ajoutNodeAParent(body, centerRotBras1);
    grapheScene.ajoutNodeAParent(body, centerRotBras2);
    grapheScene.ajoutNodeAParent(body, centerRotJambe1);
    grapheScene.ajoutNodeAParent(body, centerRotJambe2);

    //Ajout Environnement
    surface->option = CREUSE;
    surface->particular_zone.push_back(Zone(0.0f,0.0f,0.1f,1.0f,0.0f,0.3f,0.3f));

    Golden_sun->active = false;
    grapheScene.ajoutPlateforme(Golden_sun);

    grapheScene.ajoutPlateforme(minecraft_planet);

    skybox->aabb.max = glm::vec3(-FLT_MAX);
    skybox->aabb.min = glm::vec3(FLT_MAX);
    grapheScene.ajoutPlateforme(skybox);

    // Zone de safe + respawn
    grapheScene.ajoutPlateforme(protection);
    items.push_back(plat1);
    grapheScene.ajoutPlateforme(plat1); 


    // Ajout des éléments de la poule au graphe de scène
    grapheScene.ajoutNode(pouleBody);
    grapheScene.ajoutNodeAParent(pouleBody, pouleTete);
    grapheScene.ajoutNodeAParent(pouleTete, pouleBec);
    grapheScene.ajoutNodeAParent(pouleTete, pouleCrete);
    grapheScene.ajoutNodeAParent(pouleBody, pouleAile1);
    grapheScene.ajoutNodeAParent(pouleBody, pouleAile2);
    grapheScene.ajoutNodeAParent(pouleBody, poulePatte1);
    grapheScene.ajoutNodeAParent(pouleBody, poulePatte2);

    grapheScene.ajoutNodeAParent(pouleBody, barreVie);
    grapheScene.barreVie = barreVie;

    std::vector<Node*> pouleParts = {pouleBody, pouleTete, pouleBec, pouleAile1, pouleAile2, poulePatte1, poulePatte2, pouleCrete};
    grapheScene.pouleParts.assign(pouleParts.begin(), pouleParts.end());


/*Mini poulet
pouleBody2->active = true;

grapheScene.ajoutNode(pouleBody2);
grapheScene.ajoutNodeAParent(pouleBody2, pouleTete2);
grapheScene.ajoutNodeAParent(pouleTete2, pouleBec2);
grapheScene.ajoutNodeAParent(pouleBody2, pouleAile21);
grapheScene.ajoutNodeAParent(pouleBody2, pouleAile22);
grapheScene.ajoutNodeAParent(pouleBody2, poulePatte21);
grapheScene.ajoutNodeAParent(pouleBody2, poulePatte22);

grapheScene.ajoutNodeAParent(pouleBody, barreVie);
grapheScene.barreVie = barreVie;
*/
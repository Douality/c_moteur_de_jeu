#pragma once
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
glm::vec3 camera_position = glm::vec3(0.0f, 1.0f, 3.0f);
glm::vec3 camera_target = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up = glm::vec3(0.0f, 1.0f, 0.0f);

glm::vec3 object_position = glm::vec3(0.0f, 0.0f, 0.0f); // Position de l'objet 3D
float distanceFromCamera = glm::length(camera_position - object_position);

// Debug
void print3(glm::vec3 v){
    std::cout << "(" << v.x <<","<<v.y<<","<<v.z<<")"<<std::endl;
}

// timing
float deltaTime = 0.0f;    // time between current frame and last frame
float lastFrame = 0.0f;

//rotation
float angle = 0.;
float zoom = 1.;

//resolution
float resolution = 25.0f;

//ressources mod3d
std::vector<unsigned short> indices; //Triangles concaténés dans une liste
std::vector <std::vector<unsigned short>> triangles;
std::vector <glm::vec3> indexed_vertices;
std::vector <glm::vec2> uvs;
std::vector <glm::vec3> normals;

//rotation camera
float rotation_speed = 0.1f;
float rotation_angle = 0.0f;
bool is_free_camera = true;
float sun_rotation = 0.0f;
float arm_rotation = 0.0f;

//physics de base
glm::vec3 gravity(0.0f, -9.81f, 0.0f);
bool updateRotations = false;
bool inWater = false;

//Mesh option
#define CREUSE 55
#define SKYBOX 66

//Control projectile
bool shotFired = true; //tirer un seul cube à la fois
int HpPoule = 100;

int projectile_color = 0;
bool active_color_buff = false;
bool LanceProjectile = false;

//Textures
int width, height, nrChannels;
unsigned char *data = stbi_load("heightmap.png", &width, &height, &nrChannels, 0);

//Animation control
int my_music = 0;
int player_hp = 10;
bool active_map_dgt = false;
bool active_restart = false;
int jump_max = 5;
float y_jump_max = 12.0f;
int nb_jump = 0;
bool cinematique = false;

//temps
int nb_frame = 100;
int current_frame = 0;
std::random_device rd; // Utilisé pour initialiser le générateur de nombres aléatoires
std::mt19937 gen(rd()); // Générateur de nombres aléatoires basé sur Mersenne Twister
std::uniform_real_distribution<float> dis(1.0, 3.0); 

//Objectif final
bool bossIsKilled = false;
bool isThereObjectif = false;
bool inter_anim = false;
bool fin_anime = false;
bool new_music_play = false;
bool direction_star_x = false;
bool direction_star_y = false;
bool direction_star_z = false;
glm::vec3 target(3.0f,0.5f,-2.0f);
glm::vec3 last_pos(0.0f);
glm::vec3 pas;
int max_shot = 3;
bool FIN = false;

#include "../Class/structures.h"

Material default_material;
Light default_light;
Character character;

//elements
Mur_invisible mur_invisible(
                            //max
                            glm::vec3(-1.0f,-0.001f,-4.0f),
                            //min
                            glm::vec3(3.3f,jump_max,0.79f)
                            );

glm::vec3 restart_position = glm::vec3(1.0f,0.0f,0.0f);

//execution différée avec thread
void delayedExecutionBool(bool* b) {
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    *b = false;
}

//Ajout presentation
bool print_player_pv;
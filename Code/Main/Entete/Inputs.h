#pragma once
void processInput(GLFWwindow *window) {
    static bool firstMouse = true;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    //Camera zoom in and out
    float cameraSpeed = 2.5 * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
        isPlaying = false;
        std::cout << "cut" << std::endl;
    }
    glm::vec3 last_camera = camera_position;
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_target;
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_target;

    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
        arm_rotation += 0.1f;
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
        arm_rotation -= 0.1f;
    if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
        bossIsKilled = true;

    //translations
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_up;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_up;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        //camera_position -= cameraSpeed * glm::normalize(glm::cross(camera_target, camera_up));
        glm::vec3 camera_right = glm::normalize(glm::cross(camera_target - camera_position, camera_up));
        camera_position -= camera_right * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        //camera_position += cameraSpeed * glm::normalize(glm::cross(camera_target, camera_up));
        glm::vec3 camera_right = glm::normalize(glm::cross(camera_target - camera_position, camera_up));
        camera_position += camera_right * cameraSpeed;
    }
    if(camera_position.x > 30.0f || camera_position.x < -30.0f) camera_position = last_camera;
    if(camera_position.y > 15.0f || camera_position.y < -15.0f) camera_position = last_camera;
    if(camera_position.z > 15.0f || camera_position.z < -15.0f) camera_position = last_camera;

    //resolution
    if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS) {
        resolution += 1.0f;
        //surfacePlane(indices, triangles, indexed_vertices, uvs, resolution);
        std::cout << "Résolution: " << resolution << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS) {
        resolution -= 1.0f;
        if (resolution < 1.0f) {
            resolution = 1.0f;
        }
        std::cout << "Résolution: " << resolution << std::endl;
        //surfacePlane(indices, triangles, indexed_vertices, uvs, resolution);
    }

    //Play
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
        active_color_buff = false;
    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
        active_color_buff = true;

    //Lights
    if (glfwGetKey(window,GLFW_KEY_1) == GLFW_PRESS) {
        default_light.ambient += 0.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
        default_light.ambient -= 0.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
        default_light.diffuse += 0.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
        default_light.diffuse -= 0.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS) {
        default_light.specular += 0.1f;
    }
    if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS) {
        default_light.specular -= 0.1f;
    }

    //Volume
    if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
        volume--;
        Mix_VolumeMusic(volume);
    }
    if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
        volume++;
        Mix_VolumeMusic(volume);
    }
    volume = (volume < 1 ? 1 : volume);
    volume = (volume > 128 ? 128 : volume);

    //Présentation speed
    if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS) {
        HpPoule = (HpPoule < 30 ? 300 : 30);
    }
    if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
        print_player_pv = !print_player_pv;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

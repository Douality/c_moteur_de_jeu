#pragma once
    Grid.clean();
    Grid.grid_aabb.min = surface->aabb.min;
    Grid.grid_aabb.max = surface->aabb.max;
    Grid.grille.push_back(std::vector<std::vector<Cellule*>>());
    Grid.grille[0].push_back(std::vector<Cellule*>());


    Grid.add_cell(0,0,Grid.grid_aabb.min + 0.001f,Grid.grid_aabb.max - 0.001f,1.0f,0.7f,0.3f);
    Grid.add_cell(0,0,(Grid.grid_aabb.min + 0.001f)/2.0f,Grid.grid_aabb.max - (Grid.grid_aabb.max - 0.001f)/2.0f,1.0f,0.7f,0.3f);
    Grid.add_cell(0,0,Grid.grid_aabb.max - (Grid.grid_aabb.max - 0.001f)/2.0f,Grid.grid_aabb.max - 0.001f,1.0f,0.7f,0.3f); 
    Grid.add_cell(0,0,glm::vec3(-1.0f,0.15f,-3.5f),glm::vec3(7.0f,0.30f,-2.4f),0.8f,0.8f,-0.8f);

    water_cell = Grid.get_cell(0,0,3);

    Grid.elem.push_back(&water_cell);

    /*
    Grid.print_grid(0);
    Grid.print_grid(0,0);

    print3((Grid.get_cell(0,0,0))->boite.min);
    print3((Grid.get_cell(0,0,0))->boite.max);
    print3((Grid.get_cell(0,0,1))->boite.min);
    print3((Grid.get_cell(0,0,1))->boite.max);
    print3((Grid.get_cell(0,0,2))->boite.min);
    print3((Grid.get_cell(0,0,2))->boite.max);
    
    std::cout<<Grid.grille[0][0][1]->physics.coeff_friction<<std::endl;
    */
#pragma once
    GLuint Texture = loadBMP_custom("godziText.bmp");
    //GLuint Texture = loadBMP_custom("cristalline2.bmp");
    int grassWidth, grassHeight, grassChannels;
    int rockWidth, rockHeight, rockChannels;
    int snowWidth, snowHeight, snowChannels;
    int waterWidth, waterHeight, waterChannels;
    int lavaWidth, lavaHeight, lavaChannels;
    int skyboxWidth, skyboxHeight, skyboxChannels;

    unsigned char *grassData = stbi_load("grass.png", &grassWidth, &grassHeight, &grassChannels, STBI_rgb);
    unsigned char *rockData = stbi_load("rock.png", &rockWidth, &rockHeight, &rockChannels, STBI_rgb);
    unsigned char *snowData = stbi_load("snow.png", &snowWidth, &snowHeight, &snowChannels, STBI_rgb);
    unsigned char *waterData = stbi_load("cristalline2.jpg", &waterWidth, &waterHeight, &waterChannels, STBI_rgb);
    unsigned char *lavaData = stbi_load("Lava.png", &lavaWidth, &lavaHeight, &lavaChannels, STBI_rgb);
    unsigned char *skyboxData = stbi_load("Lava.png", &skyboxWidth, &skyboxHeight, &skyboxChannels, STBI_rgb);
    unsigned int grassTexture, rockTexture, snowTexture, waterTexture, lavaTexture, skyboxTexture, starTexture;
    glGenTextures(1, &grassTexture);
    glGenTextures(1, &rockTexture);
    glGenTextures(1, &snowTexture);
    glGenTextures(1, &waterTexture);
    glGenTextures(1, &lavaTexture);
    glGenTextures(1, &skyboxTexture);
    glGenTextures(1, &starTexture);
    //Get sampler
    GLuint TextureSampler = glGetUniformLocation(programID, "colorTexture");
    GLuint grassSampler = glGetUniformLocation(programID, "grass");
    GLuint rocksSampler = glGetUniformLocation(programID, "rock");
    GLuint snowsSampler = glGetUniformLocation(programID, "snow");
    GLuint waterSampler = glGetUniformLocation(programID, "water");
    GLuint lavaSampler = glGetUniformLocation(programID, "lava");
    GLuint skyboxSampler = glGetUniformLocation(programID, "skybox");
    GLuint starSampler = glGetUniformLocation(programID, "star");
    //Link texture to sampler
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture);
    glUniform1i(TextureSampler, 0);

    //grass
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, grassTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, grassWidth, grassHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 grassData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(grassSampler, 1);

    //rock
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, rockTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, rockWidth, rockHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 rockData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(rocksSampler, 2);

    //snow
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, snowTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, snowWidth, snowHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 snowData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(snowsSampler, 3);

    //water
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, waterTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, waterWidth, waterHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 waterData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(waterSampler, 4);

    //water
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, lavaTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, lavaWidth, lavaHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 lavaData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(lavaSampler, 5);

    //skybox
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, skyboxTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, skyboxWidth, skyboxHeight, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 skyboxData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(skyboxSampler, 6);

    //star
    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, starTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
      //           starData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    //glUniform1i(starSampler, 7);

    stbi_image_free(grassData);
    stbi_image_free(rockData);
    stbi_image_free(snowData);
    stbi_image_free(waterData);
    stbi_image_free(lavaData);
    stbi_image_free(skyboxData);
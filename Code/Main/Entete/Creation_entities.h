#pragma once

//création du bonhomme dans le graphe de scène
    CubeProperties *bodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));
    CubeProperties* cubePlateau = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.015f, glm::vec3(0.0f, 0.0f, 0.0f));
    CubeProperties* cubeBox = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.015f, glm::vec3(0.0f, 0.0f, 0.0f));

    body = new Node(
            Transform(glm::vec3(0.5f, 1.0f, 0.5f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.2f, 0.0f)), false, false, true, false, 0.1f, bodyProps);

    Node *tete = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.1f, 0.0f)), true, false, false, false, 0.05f, cubeProps);

    glm::vec3 bodyPosition = glm::vec3(0.0f, 0.2f, 0.0f);

    Node *jambe1 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.01f, -0.08f, 0.0f), glm::vec3(0.04f, -0.04f, 0.0f)), false, false, true, false, 0.1f,
            cubeProps);

    Node *jambe2 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.01f, -0.08f, 0.0f), glm::vec3(-0.04f, -0.04f, 0.0f)), false, false, true, false, 0.1f,
            cubeProps);

    glm::vec3 upperArmEnd1 = glm::vec3(0.065f, 0.04f, 0.0f);
    glm::vec3 upperArmEnd2 = glm::vec3(-0.065f, 0.04f, 0.0f);
    Node *bras1 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.015f, 0.0f, 0.0f), upperArmEnd1), false, false, true, false, 0.1f, cubeProps);

    Node *bras2 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.015f, 0.0f, 0.0f), upperArmEnd2), false, false, true, false, 0.1f, cubeProps);


    // Créez des nœuds de sphère pour les centres de rotation du body
    Node *centerRotBras1 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), bras1->transform.rotationCenter),
            true, false, false, false, 0.02f);

    Node *centerRotBras2 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), bras2->transform.rotationCenter),
            true, false, false, false, 0.02f);

    Node *centerRotJambe1 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), jambe1->transform.rotationCenter),
            true, false, false, false, 0.02f);

    Node *centerRotJambe2 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), jambe2->transform.rotationCenter),
            true, false, false, false, 0.02f);

    glm::mat3x3 initialArmRotation = bras1->transform.r;

// Création du corps de la poule dans le graphe de scène
    CubeProperties *pouleBodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));


    Node *pouleBody = new Node(
            Transform(glm::vec3(5.0f, 5.0f, 5.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(1.0f, 0.45f, 0.0f)), false, false, true, false, 0.1f, pouleBodyProps);

    // Création de la tête de la poule
    Node *pouleTete = new Node(
            Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.05f, -0.07f)), false, false, true, false, 0.05f, cubeProps);

    // Création du bec de la poule
    Node *pouleBec = new Node(
            Transform(glm::vec3(0.5f, 0.5f, 1.2f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.0f, -0.035f)), false, false, true, false, 0.02f, cubeProps);

    Node *pouleCrete = new Node(
        Transform(glm::vec3(0.2f, 0.65f, 0.4f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(0.0f, 0.02f, 0.0f)), false, false, true, false, 0.05f, cubeProps);


    // Création des ailes de la poule
    Node *pouleAile1 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);
    Node *pouleAile2 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);

                      
    //Mini poule
    Node *pouleTete2 = new Node(
            Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.05f, -0.07f)), false, false, true, false, 0.05f, cubeProps);

    Node *pouleBec2 = new Node(
            Transform(glm::vec3(0.5f, 0.5f, 1.2f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.0f, -0.035f)), false, false, true, false, 0.02f, cubeProps);

    Node *pouleAile21 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);

    Node *pouleAile22 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.07f, 0.0f, 0.0f)), false, false, true, false, 0.08f, cubeProps);

    Node *poulePatte21 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.01f, -0.07f, 0.0f)), false, false, true, false, 0.05f, cubeProps);

    Node *poulePatte22 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.01f, -0.07f, 0.0f)), false, false, true, false, 0.05f, cubeProps);
    

    // Création de la barre de vie de la poule
    CubeProperties *barreVieProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));

    Node *barreVie = new Node(
            Transform(glm::vec3(0.6f, 0.1f, 0.1f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.01f, -0.1f)),
            false, false, true, false, 0.1f, barreVieProps);

//Environnements
    Node *mur = new Node(
        Transform(glm::vec3(2.0f), glm::mat3x3(wall_transform), glm::vec3(-1.0f, 4.0f, -4.0f)),
        false, true, false, false, glm::vec3(0.0f, 0.0f, -1.0f));

    glm::mat4 wall_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    glm::mat4 wall_scale = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f));
    glm::mat4 wall_transform = wall_rotation * wall_scale;
    Node *arbre = new Node(
            Transform(glm::vec3(0.5f),
                    glm::mat3x3(glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f))) *
                    glm::mat3x3(glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f))),
                    glm::vec3(-0.0f, 2.0f, -2.0f)),
            false, false, false, true, glm::vec3(0.0f, 1.0f, 0.0f));

    Node *minecraft_planet = new Node(
            Transform(glm::vec3(70.0f), glm::mat3x3(1.0f), glm::vec3(2.0f, -3.6f, -0.49f)),
            false, false, true, false, 0.1f, cubePlateau);

    Node *skybox = new Node(
            Transform(glm::vec3(900.0f), glm::mat3x3(1.0f), glm::vec3(3.55f, -4.6f, 0.5f)),
            false, false, true, false, 0.1f, cubeBox);

    Node *Star = new Node(
            Transform(glm::vec3(0.25f), 
            glm::mat3x3(wall_transform * glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f))), 
            glm::vec3(1.0f, 0.0f, -4.0f)),
            false, false, false, true, glm::vec3(4.0f, 0.0f, -10.0f));

    Node *Golden_sun = new Node(
            Transform(glm::vec3(10.0f), glm::mat3x3(1.0f), glm::vec3(4.0f, 8.0f, 0.0f)),
            true, false, false, false, 0.02f, nullptr, false);

    Node *plat1 = new Node(
            Transform(glm::vec3(2.0f), glm::mat3x3(1.0f), glm::vec3(2.0f, 0.5f, -0.0f)),
            false, false, true, false, 0.1f, cubePlateau);

        protection = new Node(
                Transform(glm::vec3(7.0f), glm::mat3x3(1.0f), glm::vec3(2.0f, 0.0f, -0.0f)),
                false, false, true, false, 0.1f, cubePlateau);
   



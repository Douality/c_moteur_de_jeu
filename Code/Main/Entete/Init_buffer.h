#pragma once
// Measure speed
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);
        moveBody(*body, window);
        updatePosBody(*body, *pouleBody, deltaTime, grapheScene.platforms);

        //animation simple (non fonctionnel)
        if (updateRotations) {
            glm::mat4 rotationMat4 = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1, 0, 0));
            glm::mat3x3 rotation = glm::mat3(rotationMat4);
            bras1->transform.r = initialArmRotation * rotation;
            updateRotations = false;
        }

        Transform identityTransform(glm::vec3(1.0f), glm::mat3x3(1.0f), glm::vec3(0.0f));
        Transform absoluteBras1Transform = getAbsoluteTransform(body, bras1, identityTransform);
        jeterCube(absoluteBras1Transform, upperArmEnd1, grapheScene, pouleBody, window);
        //std::cout << "Bras global position: " << absoluteBras1Transform.t.x << ", " << absoluteBras1Transform.t.y << ", " << absoluteBras1Transform.t.z << std::endl;

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(programID);

        // Load it into a VBO
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                     GL_STATIC_DRAW);

        // Generate a buffer for the indices as well
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv0;
layout(location = 2) in vec3 N;

//layout(location = 1) in vec3 normal;
//layout(location = 3) in vec3 tangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 o_positionWorld;
out vec3 o_normalWorld;
out vec2 o_uv0;
out float altitude;
out vec3 FragPos;
out vec3 normal;

//TODO create uniform transformations matrices Model View Projection
// Values that stay constant for the whole mesh.

void main(){
    //mat3 normalMatrix = mat3(transpose(inverse(model)));
    o_uv0 = uv0;
    vec4 positionWorld = model * vec4(position, 1.0);
    FragPos = vec3(model * vec4(position, 1.0));
    normal = N;
    o_positionWorld = positionWorld.xyz;
    o_normalWorld = mat3(transpose(inverse(model))) * N;
    gl_Position = projection * view * positionWorld;
    altitude = position.y;
}
# Moteur de Jeu en C++ - Projet Développeur de Moteur

Ce projet consiste en l'implémentation d'un moteur de jeu complet en C++. Le but de ce projet est de comprendre en profondeur les mécaniques sous-jacentes d'un moteur de jeu en développant un jeu complet de zéro, y compris la gestion de la physique, la création de maillages, la gestion des objets, la gestion du son, etc.

## À Propos du Projet

Ce projet vise à être améliorer d'avantage au cours de la suite du cursus et reste une approche première essentiellemnt et de recherche

### Le Jeu : Combat de Boss

Le jeu que nous développons est un combat de boss dans un environnement entièrement conçu à la main. Tous les aspects du jeu, y compris les mécaniques de jeu, les maillages, les objets, et le son, sont créés de manière personnalisée. Il s'agit d'une expérience pratique visant à explorer en profondeur les défis et les complexités du développement de moteurs de jeu.

### Structure du Projet

Le projet est structuré en plusieurs répertoires :

- `code` : Contient le code source principal du moteur de jeu.
- `build` : Répertoire où les fichiers de construction sont générés.
- `common` : Ressources communes partagées par le moteur et le jeu.
- `external` : Bibliothèques tierces utilisées dans le projet.
- `rendus` : Contient les fichiers de rendu du jeu.
- `vidéo` : Fichiers vidéo pour la documentation.
- `BUILDING.txt` : Instructions sur la manière de construire et exécuter le projet.
- `CMakeLists.txt` : Fichier de configuration CMake.

### Comment Construire le Projet

Pour construire et exécuter le projet, suivez les instructions fournies dans le fichier `BUILDING.txt`. Assurez-vous d'avoir CMake et toutes les dépendances nécessaires installées sur votre système.

